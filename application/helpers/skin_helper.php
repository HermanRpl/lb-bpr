<?php 
defined("BASEPATH")or exit('NO DIRECT SCRIPT ALLOWED');

function view($folder,$data=NULL){
	$CI 	=& get_instance();
	$CI->load->view('skin/header',$data);
	$CI->load->view('skin/navbar');
	$CI->load->view('skin/sidebar');
	$CI->load->view($folder.'/index.php',$data);
	$CI->load->view('skin/footer');
	$CI->load->view('skin/control_sidebar');

	
}
function view3($folder,$file,$data=null){
	$CI 	=& get_instance();
	$CI->load->view('skin/header',$data);
	$CI->load->view('skin/navbar');
	$CI->load->view('skin/sidebar');
	$CI->load->view($folder.'/'.$file,$data);
	$CI->load->view('skin/footer');
	$CI->load->view('skin/control_sidebar');
}

function view2($dollar,$db=NULL){
	$CI 	=& get_instance();
	$CI->load->view('skin/header',$db);
	$CI->load->view('skin/navbar2');
	$CI->load->view('skin/sidebar2');
	$CI->load->view($dollar.'/index.php',$db);
	$CI->load->view('skin/footer');
	$CI->load->view('skin/control_sidebar');
	
}


function sidebar(){
	$arr = [
		'dash' => 'Dashboard',
		'data' => 'Form',
		'pengaturan' => 'Pengaturan'
	];

	$arr2 = [
		[
			'text'=>'Form 0000',
			'url'=>'form_0000',
			'icon'=>'fa fa-file-o'
		],
		[
			'text'=>'Form 0001',
			'url'=>'form_0001',
			'icon'=>'fa fa-file-o'
		],
		[
			'text'=>'Form 0008',
			'url'=>'form_0008',
			'icon'=>'fa fa-file-o'
		],
		[
			'text'=>'Form 0700',
			'url'=>'form_0700',
			'icon'=>'fa fa-file-o'
		],
		[
			'text'=>'Form 0800',
			'url'=>'form_0800',
			'icon'=>'fa fa-file-o'
		],
		[
			'text'=>'Form 0900',
			'url'=>'form_0900',
			'icon'=>'fa fa-file-o'
		],
		[
			'text'=>'Form 0901',
			'url'=>'form_0901',
			'icon'=>'fa fa-file-o'
		],
		[
			'text'=>'Form 1000',
			'url'=>'form_1000',
			'icon'=>'fa fa-file-o'
		],
		[
			'text'=>'Form 1100',
			'url'=>'form_1100',
			'icon'=>'fa fa-file-o'
		],
		[
			'text'=>'Form 1200',
			'url'=>'form_1200',
			'icon'=>'fa fa-file-o'
		],
		[
			'text'=>'Form 1300',
			'url'=>'form_1300',
			'icon'=>'fa fa-file-o'
		],
		[
			'text'=>'Form 1400',
			'url'=>'form_1400',
			'icon'=>'fa fa-file-o'
		],
		[
			'text'=>'Form 1401',
			'url'=>'form_1401',
			'icon'=>'fa fa-file-o'
		],
		[
			'text'=>'Form 1500',
			'url'=>'form_1500',
			'icon'=>'fa fa-file-o'
		],
	];

	?>
	<li>
		<a href="#">
			<i class="glyphicon glyphicon-home"></i> <span><?php echo $arr['dash'] ?></span>
		</a>
	</li>
	
	<li class="treeview">
		<a>
			<i class="fa fa-folder"></i>
			<span><?php echo $arr['data'] ?></span>
			<span class="pull-right-container">
				<i class="fa fa-angle-left pull-right"></i>
			</span>
		</a>
		<ul class="treeview-menu">
			<li>
				<?php foreach ($arr2 as $key): ?>
					<a href="<?php echo base_url($key['url']) ?>">
						<i class="<?php echo $key['icon'] ;?>"></i> <span><?php echo $key['text']; ?></span>
					</a>
				<?php endforeach ?>

			</li>
		</ul>
	</li>
	
	<?php 
}	

function sidebar2(){
	$url2 = site_url('index');
	$array = [
		[
			'text'=>'Kembali',
			'icon'=>'fa fa-angle-double-left fa-lg',
			'url'=>$url2
		]
	];
	?>
	<?php foreach ($array as $row): ?>
		<li>
			<a href="<?php echo $row['url'] ?>">
				<i class="<?php echo $row['icon'] ?>"></i> <span><?php echo $row['text'] ?></span>
			</a>
		</li>
	<?php endforeach ?>
	<?php 
}

function kode($i,$var){
   $CI =& get_instance();
    $query =  $CI->db->query('SELECT CONCAT("'.$var.'", LPAD(RIGHT(IFNULL(MAX(kode),0),6) +1, 6, "0")) as kode FROM '.$i.'')->row_array();
   echo $query['kode'];
}