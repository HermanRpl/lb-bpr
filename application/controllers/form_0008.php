<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Form_0008 extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$data['title'] = 'Form 0008';
		$a = 'select * from form_0008';
		$data['query'] = $this->db->query($a)->result_array();
		view('form_0008',$data);
	}
	
	public function view_add()
	{
		$data['title'] = 'Tambah Data';
		$a = 'select * from ref_sandi_pos_rincian_rasio_keuangan_triwulanan';
		$data['sandii'] = $this->db->query($a)->result_array();
		view3('form_0008','tambah',$data);
	}

	public function add()
	{
		$periode = $this->input->post('periode');
		$flag_detail = $this->input->post('flag_detail');
		$sandi_pos = $this->input->post('sandi_pos');
		$nilai_rasio= $this->input->post('nilai_rasio');
		$where = array(
			'periode' => $periode, 
			'flag_detail' => $flag_detail, 
			'sandi_pos' => $sandi_pos, 
			'nilai_rasio' => $nilai_rasio 
		);
		$this->db->insert('form_0008', $where);
		redirect('form_0008');
	}

	public function view_edit($id)
	{
		$data['title'] = 'Ubah Data';
		$a = 'select * from ref_sandi_pos_rincian_rasio_keuangan_triwulanan';
		$data['sandii'] = $this->db->query($a)->result_array();
		$this->db->where('id', $id);
		$b = "select * from form_0008 where id='".$id."'";
		$data['dataku'] = $this->db->query($b)->result_array();
		view3('form_0008','ubah',$data);
	}

	public function update()
	{
		$id = $this->input->post('id');
		$periode = $this->input->post('periode');
		$flag_detail = $this->input->post('flag_detail');
		$sandi_pos = $this->input->post('sandi_pos');
		$nilai_rasio= $this->input->post('nilai_rasio');
		$where = array(
			'periode' => $periode, 
			'flag_detail' => $flag_detail, 
			'sandi_pos' => $sandi_pos, 
			'nilai_rasio' => $nilai_rasio 
		);
		$this->db->where('id', $id);
		$this->db->update('form_0008', $where);
		redirect('form_0008');
	}

	public function delete()
	{
		$id = $this->input->post('id');
		$this->db->where('id', $id);
		$this->db->delete('form_0008');
	}
}