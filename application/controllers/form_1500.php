<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Form_1500 extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$data['title'] = 'Form 1500';
		$a = 'form_1500';
		$data['query'] = $this->db->get($a)->result_array();
		view('form_1500',$data);
	}

	public function view_add()
	{
		$data['title'] = 'Tambah Data';
		$a = 'select * from ref_jenis_aset_dihapus_buku';
		$data['jenisnya'] = $this->db->query($a)->result_array();
		$b = 'select * from ref_sandi_pihak_lawan';
		$data['goldebitur'] = $this->db->query($b)->result_array();
		$c = 'select * from ref_hubungan_dengan_bank';
		$data['hubunganbank'] = $this->db->query($c)->result_array();
		$d = 'select * from ref_jenis_agunan';
		$data['agunanjenis'] = $this->db->query($d)->result_array();
		view3('form_1500','tambah',$data);
	}

	/*public function setjenis($sandi='')
	{
		$a = "select * from ref_jenis_aset_dihapus_buku where sandi='".$sandi."'";
		$query = $this->db->query($a)->result_array();
		$keterangan = '';
		foreach ($query as $key) {
			$keterangan = $key['keterangan'];
		}
		echo json_encode(array("keterangan"=>$keterangan));
	}

	public function setgol($sandi='')
	{
		$b = "select * from ref_sandi_pihak_lawan where sandi='".$sandi."' ";
		$query = $this->db->query($b)->result_array();
		$keterangan1 = '';
		foreach ($query as $key) {
			$keterangan1 = $key['keterangan'];
		}
		echo json_encode(array("keterangan"=>$keterangan1));
	}

	public function sethubungan($sandi='')
	 {
	 	$c = "select * from ref_hubungan_dengan_bank where sandi='".$sandi."' ";
		$query = $this->db->query($c)->result_array();
		$keterangan2 = '';
		foreach ($query as $key) {
			$keterangan2 = $key['keterangan'];
		}
		echo json_encode(array("keterangan"=>$keterangan2));
	 }

	 public function setagunan($sandi='')
	 {
	 	$d = "select * from ref_jenis_agunan where sandi='".$sandi."' ";
		$query = $this->db->query($d)->result_array();
		$keterangan3 = '';
		foreach ($query as $key) {
			$keterangan3 = $key['keterangan'];
		}
		echo json_encode(array("keterangan"=>$keterangan3));
	 }*/

	public function add()
	{
		$periode = $this->input->post('periode');
		$flag_detail = $this->input->post('flag_detail');
		$sandi_kantor = $this->input->post('sandi_kantor');
		$nomor_cif = $this->input->post('nomor_cif');
		$no_rekening = $this->input->post('no_rekening');
		$jenis = $this->input->post('jenis');
		$gol_debitur = $this->input->post('gol_debitur');
		$hubungan_bank = $this->input->post('hubungan_bank');
		$tgl_hapus_buku = $this->input->post('tgl_hapus_buku');
		$sp_hapus_buku = $this->input->post('sp_hapus_buku');
		$sp_akumulasi_tertagih = $this->input->post('sp_akumulasi_tertagih');
		$sp_per_posisi_laporan = $this->input->post('sp_per_posisi_laporan');
		$tb_hapus_buku = $this->input->post('tb_hapus_buku');
		$tb_akumulasi_tertagih = $this->input->post('tb_akumulasi_tertagih');
		$tb_akumulasi_bunga_berjalan = $this->input->post('tb_akumulasi_bunga_berjalan');
		$tb_per_posisi_laporan = $this->input->post('tb_per_posisi_laporan');
		$agunan_jenis = $this->input->post('agunan_jenis');
		$agunan_alamat = $this->input->post('agunan_alamat');
		$agunan_nilai = $this->input->post('agunan_nilai');
		$array = array(
			'periode' => $periode,
			'flag_detail' => $flag_detail,
			'sandi_kantor' => $sandi_kantor,
			'nomor_cif' => $nomor_cif,
			'no_rekening' => $no_rekening,
			'jenis' => $jenis,
			'gol_debitur' => $gol_debitur,
			'hubungan_bank' => $hubungan_bank,
			'tgl_hapus_buku' => $tgl_hapus_buku,
			'sp_hapus_buku' => $sp_hapus_buku,
			'sp_akumulasi_tertagih' => $sp_akumulasi_tertagih,
			'sp_per_posisi_laporan' => $sp_per_posisi_laporan,
			'tb_hapus_buku' => $tb_hapus_buku,
			'tb_akumulasi_tertagih' => $tb_akumulasi_tertagih,
			'tb_akumulasi_bunga_berjalan' => $tb_akumulasi_bunga_berjalan,
			'tb_per_posisi_laporan' => $tb_per_posisi_laporan,
			'agunan_jenis' => $agunan_jenis,
			'agunan_alamat' => $agunan_alamat,
			'agunan_nilai' => $agunan_nilai,
		);
		$this->db->insert('form_1500', $array);
		redirect('form_1500');
	}

	public function view_edit($id)
	{
		$data['title'] = 'Ubah Data';
		$a = 'select * from ref_jenis_aset_dihapus_buku';
		$data['jenisnya'] = $this->db->query($a)->result_array();
		$b = 'select * from ref_sandi_pihak_lawan';
		$data['goldebitur'] = $this->db->query($b)->result_array();
		$c = 'select * from ref_hubungan_dengan_bank';
		$data['hubunganbank'] = $this->db->query($c)->result_array();
		$d = 'select * from ref_jenis_agunan';
		$data['agunanjenis'] = $this->db->query($d)->result_array();
		$e = "select * from form_1500 where id = '".$id."'";
		$data['data_form1500'] = $this->db->query($e)->result_array();
		view3('form_1500','ubah',$data);
	}

	public function update()
	{
		$id = $this->input->post('id');
		$periode = $this->input->post('periode');
		$flag_detail = $this->input->post('flag_detail');
		$sandi_kantor = $this->input->post('sandi_kantor');
		$nomor_cif = $this->input->post('nomor_cif');
		$no_rekening = $this->input->post('no_rekening');
		$jenis = $this->input->post('jenis');
		$gol_debitur = $this->input->post('gol_debitur');
		$hubungan_bank = $this->input->post('hubungan_bank');
		$tgl_hapus_buku = $this->input->post('tgl_hapus_buku');
		$sp_hapus_buku = $this->input->post('sp_hapus_buku');
		$sp_akumulasi_tertagih = $this->input->post('sp_akumulasi_tertagih');
		$sp_per_posisi_laporan = $this->input->post('sp_per_posisi_laporan');
		$tb_hapus_buku = $this->input->post('tb_hapus_buku');
		$tb_akumulasi_tertagih = $this->input->post('tb_akumulasi_tertagih');
		$tb_akumulasi_bunga_berjalan = $this->input->post('tb_akumulasi_bunga_berjalan');
		$tb_per_posisi_laporan = $this->input->post('tb_per_posisi_laporan');
		$agunan_jenis = $this->input->post('agunan_jenis');
		$agunan_alamat = $this->input->post('agunan_alamat');
		$agunan_nilai = $this->input->post('agunan_nilai');
		$array = array(
			'periode' => $periode,
			'flag_detail' => $flag_detail,
			'sandi_kantor' => $sandi_kantor,
			'nomor_cif' => $nomor_cif,
			'no_rekening' => $no_rekening,
			'jenis' => $jenis,
			'gol_debitur' => $gol_debitur,
			'hubungan_bank' => $hubungan_bank,
			'tgl_hapus_buku' => $tgl_hapus_buku,
			'sp_hapus_buku' => $sp_hapus_buku,
			'sp_akumulasi_tertagih' => $sp_akumulasi_tertagih,
			'sp_per_posisi_laporan' => $sp_per_posisi_laporan,
			'tb_hapus_buku' => $tb_hapus_buku,
			'tb_akumulasi_tertagih' => $tb_akumulasi_tertagih,
			'tb_akumulasi_bunga_berjalan' => $tb_akumulasi_bunga_berjalan,
			'tb_per_posisi_laporan' => $tb_per_posisi_laporan,
			'agunan_jenis' => $agunan_jenis,
			'agunan_alamat' => $agunan_alamat,
			'agunan_nilai' => $agunan_nilai,
		);
		$this->db->where('id', $id);
		$this->db->update('form_1500', $array);
		redirect('form_1500');
	}

	public function delete()
	{
		$id = $this->input->post('id');
		$this->db->where('id', $id);
		$this->db->delete('form_1500');
	}
}