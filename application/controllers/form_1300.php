<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Form_1300 extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$data['title'] = 'Form 1300';
		$a = 'select * from form_1300';
		$data['query'] = $this->db->query($a)->result_array();
		view('form_1300',$data);
	}

	public function view_add()
	{
		$data['title'] = 'Tambah Data';
		$a = 'select * from ref_sandi_pihak_lawan';
		$data['jeba'] = $this->db->query($a)->result_array();
		$b = 'select * from ref_sandi_bank';
		$data['saba'] = $this->db->query($b)->result_array();
		$c = 'select * from ref_kabupatenkota';
		$data['loba'] = $this->db->query($c)->result_array();
		$d = 'select * from ref_jenis_simpanan_dari_bank_lain';
		$data['jeniss'] = $this->db->query($d)->result_array();
		$e = 'select * from ref_hubungan_dengan_bank';
		$data['huba'] = $this->db->query($e)->result_array();
		$f = 'select * from ref_alasan_diblokir';
		$data['alasan'] = $this->db->query($f)->result_array();
		view3('form_1300','tambah',$data);
	}

	/*public function setjeba($sandi='')
	{
		$a = "select * from ref_sandi_pihak_lawan where sandi='".$sandi."'";
		$query = $this->db->query($a)->result_array();
		$keterangan = '';
		foreach ($query as $key) {
			$keterangan = $key['keterangan'];
		}
		echo json_encode(array("keterangan"=>$keterangan));
	}

	public function setsandibankk($sandi='')
	{
		$b = "select * from ref_sandi_bank where sandi='".$sandi."'";
		$query = $this->db->query($b)->result_array();
		$keterangan1 = '';
		foreach ($query as $key) {
			$keterangan1 = $key['keterangan'];
		}
		echo json_encode(array("keterangan"=>$keterangan1));
	}

	public function setlokasibankk($sandi='')
	{
		$c = 'select * from ref_kabupatenkota';
		$query = $this->db->query($c)->result_array();
		$keterangan2 = '';
		foreach ($query as $key) {
			$keterangan2 = $key['keterangan'];
		}
		echo json_encode(array("keterangan"=>$keterangan2));
	}

	public function setjenisss($sandi='')
	{
		$d = "select * from ref_jenis_simpanan_dari_bank_lain where sandi = '".$sandi."'";
		$query = $this->db->query($d)->result_array();
		$keterangan3 = '';
		foreach ($query as $key) {
			$keterangan3 = $key['keterangan'];
		}
		echo json_encode(array("keterangan"=>$keterangan3));
	}

	public function sethubunganbankk($value='')
	{
		$e = 'select * from ref_hubungan_dengan_bank';
		$query = $this->db->query($e)->result_array();
		$keterangan4 = '';
		foreach ($query as $key) {
			$keterangan4 = $key['keterangan'];
		}
		echo json_encode(array("keterangan"=>$keterangan4));
	}

	public function setalasannya($sandi='')
	{
		$f = "select * from ref_alasan_diblokir where sandi='".$sandi."'";
		$query = $this->db->query($f)->result_array();
		$keterangan5 = '';
		foreach ($query as $key) {
			$keterangan5 = $key['keterangan'];
		}
		echo json_encode(array("keterangan"=>$keterangan5));
	}*/

	public function add()
	{
		$periode = $this->input->post('periode');
		$flag_detail = $this->input->post('flag_detail');
		$sandi_kantor = $this->input->post('sandi_kantor');
		$nomor_cif = $this->input->post('nomor_cif');
		$nomor_rekening = $this->input->post('nomor_rekening');
		$jenis_bank = $this->input->post('jenis_bank');
		$sandi_bank = $this->input->post('sandi_bank');
		$lokasi_bank = $this->input->post('lokasi_bank');
		$jenis = $this->input->post('jenis');
		$hubungan_bank = $this->input->post('hubungan_bank');
		$tgl_mulai = $this->input->post('tgl_mulai');
		$tgl_jatuh_tempo = $this->input->post('tgl_jatuh_tempo');
		$suku_bunga = $this->input->post('suku_bunga');
		$nominal = $this->input->post('nominal');
		$nominal_diblokir = $this->input->post('nominal_diblokir');
		$alasan_diblokir = $this->input->post('alasan_diblokir');
		$transaksi_belum_diamortisasi = $this->input->post('transaksi_belum_diamortisasi');
		$jumlah = $this->input->post('jumlah');
		$array = array(
			'periode' => $periode, 
			'flag_detail' => $flag_detail, 
			'sandi_kantor' => $sandi_kantor, 
			'nomor_cif' => $nomor_cif, 
			'nomor_rekening' => $nomor_rekening, 
			'jenis_bank' => $jenis_bank, 
			'sandi_bank' => $sandi_bank, 
			'lokasi_bank' => $lokasi_bank, 
			'jenis' => $jenis, 
			'hubungan_bank' => $hubungan_bank,  
			'tgl_mulai' => $tgl_mulai, 
			'tgl_jatuh_tempo' => $tgl_jatuh_tempo, 
			'suku_bunga' => $suku_bunga, 
			'nominal' => $nominal, 
			'nominal_diblokir' => $nominal_diblokir, 
			'alasan_diblokir' => $alasan_diblokir, 
			'transaksi_belum_diamortisasi' => $transaksi_belum_diamortisasi, 
			'jumlah' => $jumlah 
		);
		$this->db->insert('form_1300', $array);
		redirect('form_1300');
	}

	public function view_edit($id)
	{
		$data['title'] = 'Ubah Data';
		$a = 'select * from ref_sandi_pihak_lawan';
		$data['jeba'] = $this->db->query($a)->result_array();
		$b = 'select * from ref_sandi_bank';
		$data['saba'] = $this->db->query($b)->result_array();
		$c = 'select * from ref_kabupatenkota';
		$data['loba'] = $this->db->query($c)->result_array();
		$d = 'select * from ref_jenis_simpanan_dari_bank_lain';
		$data['jeniss'] = $this->db->query($d)->result_array();
		$e = 'select * from ref_hubungan_dengan_bank';
		$data['huba'] = $this->db->query($e)->result_array();
		$f = 'select * from ref_alasan_diblokir';
		$data['alasan'] = $this->db->query($f)->result_array();
		$h = "select * from form_1300 where id='".$id."'";
		$data['data_ubah'] = $this->db->query($h)->result_array();
		view3('form_1300','ubah',$data);
	}

	public function update()
	{
		$id = $this->input->post('id');
		$periode = $this->input->post('periode');
		$flag_detail = $this->input->post('flag_detail');
		$sandi_kantor = $this->input->post('sandi_kantor');
		$nomor_cif = $this->input->post('nomor_cif');
		$nomor_rekening = $this->input->post('nomor_rekening');
		$jenis_bank = $this->input->post('jenis_bank');
		$sandi_bank = $this->input->post('sandi_bank');
		$lokasi_bank = $this->input->post('lokasi_bank');
		$jenis = $this->input->post('jenis');
		$hubungan_bank = $this->input->post('hubungan_bank');
		$tgl_mulai = $this->input->post('tgl_mulai');
		$tgl_jatuh_tempo = $this->input->post('tgl_jatuh_tempo');
		$suku_bunga = $this->input->post('suku_bunga');
		$nominal = $this->input->post('nominal');
		$nominal_diblokir = $this->input->post('nominal_diblokir');
		$alasan_diblokir = $this->input->post('alasan_diblokir');
		$transaksi_belum_diamortisasi = $this->input->post('transaksi_belum_diamortisasi');
		$jumlah = $this->input->post('jumlah');
		$array = array(
			'periode' => $periode, 
			'flag_detail' => $flag_detail, 
			'sandi_kantor' => $sandi_kantor, 
			'nomor_cif' => $nomor_cif, 
			'nomor_rekening' => $nomor_rekening, 
			'jenis_bank' => $jenis_bank, 
			'sandi_bank' => $sandi_bank, 
			'lokasi_bank' => $lokasi_bank, 
			'jenis' => $jenis, 
			'hubungan_bank' => $hubungan_bank,  
			'tgl_mulai' => $tgl_mulai, 
			'tgl_jatuh_tempo' => $tgl_jatuh_tempo, 
			'suku_bunga' => $suku_bunga, 
			'nominal' => $nominal, 
			'nominal_diblokir' => $nominal_diblokir, 
			'alasan_diblokir' => $alasan_diblokir, 
			'transaksi_belum_diamortisasi' => $transaksi_belum_diamortisasi, 
			'jumlah' => $jumlah 
		);

		$this->db->where('id', $id);
		$this->db->update('form_1300', $array);
		redirect('form_1300');
	}

	public function delete()
	{
		$id = $this->input->post('id');
		$this->db->where('id', $id);
		$this->db->delete('form_1300');
	}
}