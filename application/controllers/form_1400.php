<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Form_1400 extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$data['title'] = 'Form 1400';
		$a = 'select * from form_1400';
		$data['query'] = $this->db->query($a)->result_array();
		view('form_1400',$data);
	}

	public function view_add()
	{
		$data['title'] = 'Tambah Data';
		$a = 'select * from ref_sandi_pos_rincian_liabilitas_lainnya';
		$data['sandinya'] = $this->db->query($a)->result_array();
		view3('form_1400','tambah',$data);
	}

	/*public function setsandipos($sandi='')
	{
		$a = "select * from ref_sandi_pos_rincian_liabilitas_lainnya where sandi='".$sandi."'";
		$query = $this->db->query($a)->result_array();
		$Keterangan = '';
		foreach ($query as $key) {
				$keterangan = $key['keterangan'];
			}
			echo json_encode(array("keterangan"=>$keterangan));
	}*/

	public function add()
	{
		$periode = $this->input->post('periode');
		$flag_detail = $this->input->post('flag_detail');
		$sandi_kantor = $this->input->post('sandi_kantor');
		$sandi_pos = $this->input->post('sandi_pos');
		$jumlah = $this->input->post('jumlah');
		$array = array(
			'periode' => $periode, 
			'flag_detail' => $flag_detail, 
			'sandi_kantor' => $sandi_kantor, 
			'sandi_pos' => $sandi_pos, 
			'jumlah' => $jumlah 
		);
		$this->db->insert('form_1400', $array);
		redirect('form_1400');
	}

	public function view_edit($id)
	{
		$data['title'] = 'Ubah Data';
		$a = 'select * from ref_sandi_pos_rincian_liabilitas_lainnya';
		$data['sandinya'] = $this->db->query($a)->result_array();
		$b = "select * from form_1400 where id='".$id."'";
		$data['data_ubah'] = $this->db->query($b)->result_array();
		view3('form_1400','ubah',$data);
	}

	public function update()
	{
		$id = $this->input->post('id');
		$periode = $this->input->post('periode');
		$flag_detail = $this->input->post('flag_detail');
		$sandi_kantor = $this->input->post('sandi_kantor');
		$sandi_pos = $this->input->post('sandi_pos');
		$jumlah = $this->input->post('jumlah');
		$array = array(
			'periode' => $periode, 
			'flag_detail' => $flag_detail, 
			'sandi_kantor' => $sandi_kantor, 
			'sandi_pos' => $sandi_pos, 
			'jumlah' => $jumlah 
		);
		$this->db->where('id', $id);
		$this->db->update('form_1400', $array);
		redirect('form_1400');
	}

	public function delete()
	{
		$id = $this->input->post('id');
		$this->db->where('id', $id);
		$this->db->delete('form_1400');
	}
}