<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Form_0800 extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$data['title'] = 'Form_0800';
		$aset = 'select * from ref_aset_tetap_inventaris_tidak_berwujud';
		$data['jenisaset'] = $this->db->query($aset)->result_array();
		$perolehan = 'select * from ref_sumber_perolehan_aset_tidak_berwujud';
		$data['sumberperolehan'] = $this->db->query($perolehan)->result_array();
		$a = "select * from ref_status_aset_tetap_tidak_berwujud";
		$data['statusaset'] = $this->db->query($a)->result_array();
		$data['query'] = $this->db->query('select * from form_0800')->result_array();
		view('form_0800',$data);
	}

	public function read(){
		$no = 1;
		foreach ($query as $key) {
			?>
			
			<?php
		}	
	}

	/*public function setData($sandi = ''){
		$set = "SELECT * from ref_aset_tetap_inventaris_tidak_berwujud where sandi = '".$sandi."' ";
		$query = $this->db->query($set)->result_array();
		$keterangan1 = '';
		foreach ($query as $row) {
			$keterangan1 = $row['keterangan'];
		}
		echo json_encode(array( "keterangan"=>$keterangan1));
	}

	public function setperolehan($sandi = ''){
		$abc = "select * from ref_sumber_perolehan_aset_tidak_berwujud where sandi ='".$sandi."' ";
		$query = $this->db->query($abc)->result_array();
		$keterangan2 = '';
		foreach ($query as $row) {
			$keterangan2 = $row['keterangan'];
		}
		echo json_encode(array( "keterangan"=>$keterangan2));
	}

	public function setstatus($sandi = '')
	{
		$b = "select * from ref_status_aset_tetap_tidak_berwujud where sandi ='".$sandi."'";
		$query = $this->db->query($b)->result_array();
		$keterangan3 = '';
		foreach ($query as $row) {
			$keterangan3 = $row['keterangan'];
		}	
		echo json_encode(array("keterangan"=>$keterangan3));
	}*/

	public function updateform($id)
	{
		$data['title'] = 'Ubah Data';
		$array = array('id' => $id);
		$data['data_table'] = $this->db->query("select id,periode,flag_detail,sandi_kantor,jenis_aset,sumber_perolehan,status_aset,biaya_perolehan,akumulasi_penyusutan_amortisasi,akumulasi_kerugian_penurunan_nilai,nilai_tercatat from form_0800 where id ='".$id."'")->result_array();
		$aset = 'select * from ref_aset_tetap_inventaris_tidak_berwujud';
		$data['jenisaset'] = $this->db->query($aset)->result_array();
		$perolehan = 'select * from ref_sumber_perolehan_aset_tidak_berwujud';
		$data['sumberperolehan'] = $this->db->query($perolehan)->result_array();
		$a = "select * from ref_status_aset_tetap_tidak_berwujud";
		$data['statusaset'] = $this->db->query($a)->result_array();
		view3('form_0800','ubah',$data);
	}

	public function edit()
	{
		$id = $this->input->post('id');
		$periode = $this->input->post('periode');
		$flag_detail = $this->input->post('flag_detail');
		$sandi_kantor = $this->input->post('sandi_kantor');
		$jenis_aset = $this->input->post('jenis_aset');
		$sumber_perolehan = $this->input->post('sumber_perolehan');
		$status_aset = $this->input->post('status_aset');
		$biaya_perolehan = $this->input->post('biaya_perolehan');
		$akumulasi_penyusutan = $this->input->post('akumulasi_penyusutan');
		$akumulasi_kerugian = $this->input->post('akumulasi_kerugian');
		$nilai_tercatat = $this->input->post('nilai_tercatat');
		$array = array(
			'periode'=>$this->input->post('periode'),
			'flag_detail'=>$this->input->post('flag_detail'),
			'sandi_kantor'=>$this->input->post('sandi_kantor'),
			'jenis_aset'=>$this->input->post('jenis_aset'),
			'sumber_perolehan'=>$this->input->post('sumber_perolehan'),
			'status_aset'=>$this->input->post('status_aset'),
			'biaya_perolehan'=>$this->input->post('biaya_perolehan'),
			'akumulasi_penyusutan_amortisasi'=>$this->input->post('akumulasi_penyusutan'),
			'akumulasi_kerugian_penurunan_nilai'=>$this->input->post('akumulasi_kerugian'),
			'nilai_tercatat'=>$this->input->post('nilai_tercatat')
		);
		$this->db->where('id', $id);
		$this->db->update('form_0800', $array);
		redirect('form_0800');
	}

	public function save(){
		$uri3 = $this->uri->segment(3);
		$uri4 = $this->uri->segment(4);
		$arr = array(
			'periode'=>$this->input->post('periode'),
			'flag_detail'=>$this->input->post('flag_detail'),
			'sandi_kantor'=>$this->input->post('sandi_kantor'),
			'jenis_aset'=>$this->input->post('jenis_aset'),
			'sumber_perolehan'=>$this->input->post('sumber_perolehan'),
			'status_aset'=>$this->input->post('status_aset'),
			'biaya_perolehan'=>$this->input->post('biaya_perolehan'),
			'akumulasi_penyusutan_amortisasi'=>$this->input->post('akumulasi_penyusutan'),
			'akumulasi_kerugian_penurunan_nilai'=>$this->input->post('akumulasi_kerugian'),
			'nilai_tercatat'=>$this->input->post('nilai_tercatat')
		);
		if ($uri3 == 'tambah') {	
			$this->db->insert('form_0800',$arr);
			redirect();
		}
		if ($uri3 == 'hapus') {
			$this->db->query('DELETE from form_0800 where id = "'.$uri4.'"');
			redirect();		
		}
	}
}