<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Form_0700 extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$data['title'] = 'Form 0700';
		$a = 'select * from form_0700';
		$data['query'] = $this->db->query($a)->result_array();
		view('form_0700',$data);
	}

	public function view_add()
	{
		$data['title'] = 'Tambah Data';
		$a = 'select * from ref_jenis_agunan_diambilalih';
		$data['jenisagunan'] = $this->db->query($a)->result_array();
		view3('form_0700','tambah',$data);
	}

	public function add()
	{
		$periode = $this->input->post('periode');	
		$flag_detail = $this->input->post('flag_detail');	
		$sandi_kantor = $this->input->post('sandi_kantor');	
		$jenis_agunan = $this->input->post('jenis_agunan');	
		$alamat_agunan = $this->input->post('alamat_agunan');	
		$tgl_pengambilalihan = $this->input->post('tgl_pengambilalihan');	
		$nilai_pengakuan_awal = $this->input->post('nilai_pengakuan_awal');	
		$akumulasi_kerugian_penurunan = $this->input->post('akumulasi_kerugian_penurunan');	
		$jumlah = $this->input->post('jumlah');
		$array = array(
			'periode' => $periode, 
			'flag_detail' => $flag_detail, 
			'sandi_kantor' => $sandi_kantor, 
			'jenis_agunan' => $jenis_agunan, 
			'alamat_agunan' => $alamat_agunan, 
			'tgl_pengambilalihan' => $tgl_pengambilalihan, 
			'nilai_pengakuan_awal' => $nilai_pengakuan_awal, 
			'akumulasi_kerugian_penurunan' => $akumulasi_kerugian_penurunan, 
			'jumlah' => $jumlah 
		);
		$this->db->insert('form_0700', $array);
		redirect('form_0700');
	}

	public function view_edit($id)
	{
		$data['title'] = 'Form 0700';
		$a = "select * from form_0700 where id ='".$id."'";
		$data['datanya'] = $this->db->query($a)->result_array();
		$b = 'select * from ref_jenis_agunan_diambilalih';
		$data['jenisagunan'] = $this->db->query($b)->result_array();
		view3('form_0700','ubah',$data);
	}

	public function update()
	{
		$id = $this->input->post('id');	
		$periode = $this->input->post('periode');	
		$flag_detail = $this->input->post('flag_detail');	
		$sandi_kantor = $this->input->post('sandi_kantor');	
		$jenis_agunan = $this->input->post('jenis_agunan');	
		$alamat_agunan = $this->input->post('alamat_agunan');	
		$tgl_pengambilalihan = $this->input->post('tgl_pengambilalihan');	
		$nilai_pengakuan_awal = $this->input->post('nilai_pengakuan_awal');	
		$akumulasi_kerugian_penurunan = $this->input->post('akumulasi_kerugian_penurunan');	
		$jumlah = $this->input->post('jumlah');
		$array = array(
			'periode' => $periode, 
			'flag_detail' => $flag_detail, 
			'sandi_kantor' => $sandi_kantor, 
			'jenis_agunan' => $jenis_agunan, 
			'alamat_agunan' => $alamat_agunan, 
			'tgl_pengambilalihan' => $tgl_pengambilalihan, 
			'nilai_pengakuan_awal' => $nilai_pengakuan_awal, 
			'akumulasi_kerugian_penurunan' => $akumulasi_kerugian_penurunan, 
			'jumlah' => $jumlah 
		);
		$this->db->where('id', $id);
		$this->db->update('form_0700', $array);
		redirect('form_0700');
	}

	public function delete()
	{
		$id = $this->input->post('id');
		$$this->db->where('id', $id);
		$this->db->delete('form_0700');
	}
}