<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Form_1100 extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$data['title'] = 'Form 1100';
		$data['query'] = $this->db->get('form_1100')->result_array();
		view('form_1100',$data);
	}

	public function view_add()
	{
		$data['title'] = 'Tambah Data';
		$a = 'select * from ref_jenis_tabungan';
		$data['jenis'] = $this->db->query($a)->result_array();
		$b = 'select * from ref_hubungan_dengan_bank';
		$data['hubung'] = $this->db->query($b)->result_array();
		$c = 'select * from ref_sandi_pihak_lawan';
		$data['golong'] = $this->db->query($c)->result_array();
		$d = 'select * from ref_kabupatenkota';
		$data['lok'] = $this->db->query($d)->result_array();
		$e = 'select * from ref_alasan_diblokir';
		$data['alasan'] = $this->db->query($e)->result_array();
		view3('form_1100','tambah',$data);
	}

	/*public function setjenis($sandi='')
	{
		$jen = "select * from ref_jenis_tabungan where sandi = '".$sandi."' ";
		$query = $this->db->query($jen)->result_array();
		$keterangan1 = '';
		foreach ($query as $key) {
			$keterangan1 = $key['keterangan'];
		}
		echo json_encode(array("keterangan"=>$keterangan1));
	}

	public function setalas($sandi='')
	{
		$al = "select * from ref_alasan_diblokir where sandi='".$sandi."'";
		$query = $this->db->query($al)->result_array();
		$keterangan5 = '';
		foreach ($query as $key) {
			$keterangan5 = $key['keterangan'];
		}
		echo json_encode(array("keterangan"=>$keterangan5));
	}

	public function sethubungan($sandi='')
	{
		$hub = "select * from ref_hubungan_dengan_bank where sandi='".$sandi."'";
		$query = $this->db->query($hub)->result_array();
		$keterangan2 = '';
		foreach ($query as $key) {
			$keterangan2 = $key['keterangan'];
		}
		echo json_encode(array("keterangan"=>$keterangan2));
	}

	public function setgolongan($sandi='')
	{
		$gol = "select * from ref_sandi_pihak_lawan where sandi='".$sandi."'";
		$query = $this->db->query($gol)->result_array();
		$keterangan3 = '';
		foreach ($query as $key) {
			$keterangan3 = $key['keterangan'];
		}
		echo json_encode(array("keterangan"=>$keterangan3));
	}

	public function setlokasi($sandi='')
	{
		$lo = "select * from ref_kabupatenkota where sandi='".$sandi."'";
		$query = $this->db->query($lo)->result_array();
		$keterangan4 = '';
		foreach ($query as $key) {
			$keterangan4 = $key['keterangan'];
		}
		echo json_encode(array("keterangan"=>$keterangan4));
	}*/

	public function add()
	{
		$periode = $this->input->post('periode');
		$flag_detail = $this->input->post('flag_detail');
		$sandi_kantor = $this->input->post('sandi_kantor');
		$nomor_cif = $this->input->post('nomor_cif');
		$nomor_rekening = $this->input->post('nomor_rekening');
		$jenis = $this->input->post('jenis');
		$hubungan_bank = $this->input->post('hubungan_bank');
		$golongan_nasabah = $this->input->post('golongan_nasabah');
		$lokasi_nasabah = $this->input->post('lokasi_nasabah');
		$jw_tgl_mulai = $this->input->post('jw_tgl_mulai');
		$jw_tgl_jatuh_tempo = $this->input->post('jw_tgl_jatuh_tempo');
		$suku_bunga = $this->input->post('suku_bunga');
		$nominal = $this->input->post('nominal');
		$nominal_diblokir = $this->input->post('nominal_diblokir');
		$alasan_diblokir = $this->input->post('alasan_diblokir');
		$biaya_transaksi = $this->input->post('biaya_transaksi');
		$jumlah = $this->input->post('jumlah');
		$array = array(
			'periode' => $periode, 
			'flag_detail' => $flag_detail, 
			'sandi_kantor' => $sandi_kantor, 
			'nomor_cif' => $nomor_cif, 
			'nomor_rekening' => $nomor_rekening, 
			'jenis' => $jenis, 
			'hubungan_bank' => $hubungan_bank, 
			'golongan_nasabah' => $golongan_nasabah, 
			'lokasi_nasabah' => $lokasi_nasabah, 
			'jw_tgl_mulai' => $jw_tgl_mulai, 
			'jw_tgl_jatuh_tempo' => $jw_tgl_jatuh_tempo, 
			'suku_bunga' => $suku_bunga, 
			'nominal' => $nominal, 
			'nominal_diblokir' => $nominal_diblokir, 
			'alasan_diblokir' => $alasan_diblokir, 
			'biaya_transaksi_belum_diamortisasi' => $biaya_transaksi, 
			'jumlah' => $jumlah 
		);
		$this->db->insert('form_1100', $array);
		redirect('form_1100');
	}

	public function view_edit($id)
	{
		$data['title'] = 'Ubah Data';
		$a = 'select * from ref_jenis_tabungan';
		$data['jenis'] = $this->db->query($a)->result_array();
		$b = 'select * from ref_hubungan_dengan_bank';
		$data['hubung'] = $this->db->query($b)->result_array();
		$c = 'select * from ref_sandi_pihak_lawan';
		$data['golong'] = $this->db->query($c)->result_array();
		$d = 'select * from ref_kabupatenkota';
		$data['lok'] = $this->db->query($d)->result_array();
		$e = 'select * from ref_alasan_diblokir';
		$data['alasan'] = $this->db->query($e)->result_array();
		$up = "select * from form_1100 where id = '".$id."'";
		$data['nilai_ubah'] = $this->db->query($up)->result();
		view3('form_1100','ubah',$data);
	}

	public function update()
	{
		$id = $this->input->post('id');
		$periode = $this->input->post('periode');
		$flag_detail = $this->input->post('flag_detail');
		$sandi_kantor = $this->input->post('sandi_kantor');
		$nomor_cif = $this->input->post('nomor_cif');
		$nomor_rekening = $this->input->post('nomor_rekening');
		$jenis = $this->input->post('jenis');
		$hubungan_bank = $this->input->post('hubungan_bank');
		$golongan_nasabah = $this->input->post('golongan_nasabah');
		$lokasi_nasabah = $this->input->post('lokasi_nasabah');
		$jw_tgl_mulai = $this->input->post('jw_tgl_mulai');
		$jw_tgl_jatuh_tempo = $this->input->post('jw_tgl_jatuh_tempo');
		$suku_bunga = $this->input->post('suku_bunga');
		$nominal = $this->input->post('nominal');
		$nominal_diblokir = $this->input->post('nominal_diblokir');
		$alasan_diblokir = $this->input->post('alasan_diblokir');
		$biaya_transaksi = $this->input->post('biaya_transaksi');
		$jumlah = $this->input->post('jumlah');
		$array = array(
			'periode' => $periode, 
			'flag_detail' => $flag_detail, 
			'sandi_kantor' => $sandi_kantor, 
			'nomor_cif' => $nomor_cif, 
			'nomor_rekening' => $nomor_rekening, 
			'jenis' => $jenis, 
			'hubungan_bank' => $hubungan_bank, 
			'golongan_nasabah' => $golongan_nasabah, 
			'lokasi_nasabah' => $lokasi_nasabah, 
			'jw_tgl_mulai' => $jw_tgl_mulai, 
			'jw_tgl_jatuh_tempo' => $jw_tgl_jatuh_tempo, 
			'suku_bunga' => $suku_bunga, 
			'nominal' => $nominal, 
			'nominal_diblokir' => $nominal_diblokir, 
			'alasan_diblokir' => $alasan_diblokir, 
			'biaya_transaksi_belum_diamortisasi' => $biaya_transaksi, 
			'jumlah' => $jumlah 
		);
		$this->db->where('id', $id);
		$this->db->update('form_1100', $array);
		redirect('form_1100');
	}

	public function delete()
	{
		$id = $this->input->post('id');
		$this->db->where('id', $id);
		$this->db->delete('form_1100');
	}
}