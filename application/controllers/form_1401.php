<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Form_1401 extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$data['title'] = 'Form 1401';
		$a = 'select * from form_1401';
		$data['query'] = $this->db->query($a)->result_array();
		view('form_1401',$data);
	}

	public function view_add()
	{
		$data['title'] = 'Tambah Data';
		view3('form_1401','tambah',$data);
	}

	public function add()
	{
		$periode = $this->input->post('periode');
		$flag_detail = $this->input->post('flag_detail');
		$sandi_kantor = $this->input->post('sandi_kantor');
		$uraian = $this->input->post('uraian');
		$jumlah = $this->input->post('jumlah');
		$array = array(
			'periode' => $periode, 
			'flag_detail' => $flag_detail, 
			'sandi_kantor' => $sandi_kantor, 
			'uraian' => $uraian, 
			'jumlah' => $jumlah, 
		);
		$this->db->insert('form_1401', $array);
		redirect('form_1401');
	}

	public function view_edit($id)
	{
		$data['title'] = 'Ubah Data';
		$a = "select * from form_1401 where id ='".$id."'";
		$data['query'] = $this->db->query($a)->result_array();
		view3('form_1401','ubah',$data);
	}

	public function update()
	{
		$id = $this->input->post('id');
		$periode = $this->input->post('periode');
		$flag_detail = $this->input->post('flag_detail');
		$sandi_kantor = $this->input->post('sandi_kantor');
		$uraian = $this->input->post('uraian');
		$jumlah = $this->input->post('jumlah');
		$array = array(
			'periode' => $periode, 
			'flag_detail' => $flag_detail, 
			'sandi_kantor' => $sandi_kantor, 
			'uraian' => $uraian, 
			'jumlah' => $jumlah, 
		);
		$this->db->where('id', $id);
		$this->db->update('form_1401', $array);
		redirect('form_1401');
	}

	public function delete()
	{
		$id = $this->input->post('id');
		$this->db->where('id', $id);
		$this->db->delete('form_1401');
	}
}