<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Form_1000 extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$data['title'] = 'Index';
		$data['query'] = $this->db->get('form_1000')->result_array();
		view('form_1000',$data);
	}

	public function view_add()
	{
		$data['title'] = 'Tambah Data';
		$sandi = 'select * from ref_sandi_pos_rincian_liabilitas_lainnya';
		$data['sandi'] = $this->db->query($sandi)->result_array();
		view3('form_1000','tambah',$data);
	}

	public function add()
	{
		$periode = $this->input->post('periode');
		$flag_detail = $this->input->post('flag_detail');
		$sandi_kantor = $this->input->post('sandi_kantor');
		$sandi_pos = $this->input->post('sandi_pos');
		$jumlah = $this->input->post('jumlah');
		$array = array(
			'periode' => $periode, 
			'flag_detail' => $flag_detail, 
			'sandi_kantor' => $sandi_kantor, 
			'sandi_pos' => $sandi_pos, 
			'jumlah' => $jumlah 
		);
		$this->db->insert('form_1000', $array);
		redirect('form_1000');
	}

	/*public function setdata($sandi = ''){
		$abc = "select * from ref_sandi_pos_rincian_liabilitas_lainnya where sandi ='".$sandi."' ";
		$datanya = $this->db->query($abc)->result_array();
		$keterangan1 = '';
		foreach ($datanya as $row) {
			$keterangan1 = $row['keterangan'];
		}
		echo json_encode(array( "keterangan"=>$keterangan1));
	}
*/
	public function delete()
	{
		$id = $this->input->post('id');
		$this->db->where('id', $id);
		$this->db->delete('form_1000');
	}

	public function view_edit($id)
	{
		$data['title'] = 'Ubah Data';
		$sandi = 'select * from ref_sandi_pos_rincian_liabilitas_lainnya';
		$data['sandi'] = $this->db->query($sandi)->result_array();
		$where = array('id' => $id);
		$data['query'] = $this->db->get_where('form_1000',$where)->result();
		view3('form_1000','ubah',$data);
	}

	public function update()
	{
		$id = $this->input->post('id');
		$periode = $this->input->post('periode');
		$flag_detail = $this->input->post('flag_detail');
		$sandi_kantor = $this->input->post('sandi_kantor');
		$sandi_pos = $this->input->post('sandi_pos');
		$jumlah = $this->input->post('jumlah');
		$array = array(
			'periode' => $periode, 
			'flag_detail' => $flag_detail, 
			'sandi_kantor' => $sandi_kantor, 
			'sandi_pos' => $sandi_pos, 
			'jumlah' => $jumlah 
		);
		$this->db->where('id', $id);
		$this->db->update('form_1000', $array);
		redirect('form_1000');
	}
}