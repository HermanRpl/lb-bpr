<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Form_0900 extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$data['title'] = 'Form 0900';
		$data['abcd'] = $this->db->get('form_0900')->result_array();
		view('Form_0900',$data);
	}

	public function add()
	{
		$data['title'] = 'Tambah Data';
		$abc = "select * from ref_sandi_pos_rincian_aset_lainnya";
		$data['sandipos'] = $this->db->query($abc)->result_array();
		view3('form_0900','tambah',$data);
	}

	public function tambah_data()
	{
		$periode = $this->input->post('periode');
		$flag_detail = $this->input->post('flag_detail');
		$sandi_kantor = $this->input->post('sandi_kantor');
		$sandi_pos = $this->input->post('sandi_pos');
		$jumlah = $this->input->post('jumlah');
		$array = array(
			'periode' => $periode, 
			'flag_detail' => $flag_detail, 
			'sandi_kantor' => $sandi_kantor, 
			'sandi_pos' => $sandi_pos, 
			'jumlah' => $jumlah 
		);
		$this->db->insert('form_0900', $array);
		redirect('form_0900');
	}

	public function ubah($id)
	{
		$data['title'] = 'Ubah Data';
		$abc = "select * from ref_sandi_pos_rincian_aset_lainnya";
		$data['sandipos'] = $this->db->query($abc)->result_array();
		$where = array('id' => $id);
		$fc = 'form_0900';
		$data['query'] = $this->db->get_where($fc,$where)->result();
		view3('form_0900','ubah',$data);
	}

	public function ubah_data()
	{
		$id = $this->input->post('id');
		$periode = $this->input->post('periode');
		$flag_detail = $this->input->post('flag_detail');
		$sandi_kantor = $this->input->post('sandi_kantor');
		$sandi_pos = $this->input->post('sandi_pos');
		$jumlah = $this->input->post('jumlah');
		$array = array(
			'periode' => $periode, 
			'flag_detail' => $flag_detail, 
			'sandi_kantor' => $sandi_kantor, 
			'sandi_pos' => $sandi_pos, 
			'jumlah' => $jumlah 
		);
		$this->db->where('id', $id);
		$this->db->update('form_0900', $array);
		redirect('form_0900');
	}

	public function delete()
	{
		$id = $this->input->post('id');
		$this->db->where('id', $id);
		$this->db->delete('form_0900');
	}

	/*public function setdata($sandi = ''){
		$abc = "select * from ref_sandi_pos_rincian_aset_lainnya where sandi ='".$sandi."' ";
		$query = $this->db->query($abc)->result_array();
		$keterangan1 = '';
		foreach ($query as $row) {
			$keterangan1 = $row['keterangan'];
		}
		echo json_encode(array( "keterangan"=>$keterangan1));
	}*/
}