<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Form_0001 extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$data['title'] = 'Form 0001';
		$a = 'select * from form_0001';
		$data['query'] = $this->db->query($a)->result_array();
		view('Form_0001',$data);
	}

	public function view_add()
	{
		$data['title'] = 'Tambah Data';
		$a = 'select * from ref_jenis_pemilik';
		$data['jenis'] = $this->db->query($a)->result_array();
		view3('form_0001','tambah',$data);
	}

	public function add()
	{
		$periode = $this->input->post('periode');
		$flag_detail = $this->input->post('flag_detail');
		$nama = $this->input->post('nama');
		$alamat = $this->input->post('alamat');
		$jenis = $this->input->post('jenis');
		$no_identitas = $this->input->post('no_identitas');
		$psp = $this->input->post('psp');
		$jumlah_nominal = $this->input->post('jumlah_nominal');
		$persentase_kepemilikan = $this->input->post('persentase_kepemilikan');
		$array = array(
			'periode' => $periode,
			'flag_detail' => $flag_detail,
			'nama' => $nama,
			'alamat' => $alamat,
			'jenis' => $jenis,
			'no_identitas' => $no_identitas,
			'psp' => $psp,
			'jumlah_nominal' => $jumlah_nominal,
			'persentase_kepemilikan' => $persentase_kepemilikan,
		);
		$this->db->insert('form_0001', $array);
		redirect('form_0001');
	}

	public function view_edit($id)
	{
		$data['title'] = 'Ubah Data';
		$a = 'select * from ref_jenis_pemilik';
		$data['jenis'] = $this->db->query($a)->result_array();
		$b = "select * from form_0001 where id='".$id."'";
		$data['query'] = $this->db->query($b)->result_array();
		view3('form_0001','ubah',$data);
	}

	public function update()
	{
		$id = $this->input->post('id');
		$periode = $this->input->post('periode');
		$flag_detail = $this->input->post('flag_detail');
		$nama = $this->input->post('nama');
		$alamat = $this->input->post('alamat');
		$jenis = $this->input->post('jenis');
		$no_identitas = $this->input->post('no_identitas');
		$psp = $this->input->post('psp');
		$jumlah_nominal = $this->input->post('jumlah_nominal');
		$persentase_kepemilikan = $this->input->post('persentase_kepemilikan');
		$array = array(
			'periode' => $periode,
			'flag_detail' => $flag_detail,
			'nama' => $nama,
			'alamat' => $alamat,
			'jenis' => $jenis,
			'no_identitas' => $no_identitas,
			'psp' => $psp,
			'jumlah_nominal' => $jumlah_nominal,
			'persentase_kepemilikan' => $persentase_kepemilikan,
		);
		$this->db->where('id', $id);
		$this->db->update('form_0001', $array);
		redirect('form_0001');
	}

	public function delete()
	{
		$id = $this->input->post('id');
		$this->db->where('id', $id);
		$this->db->delete('form_0001');
	}
}