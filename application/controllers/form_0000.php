<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Form_0000 extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$data['title'] = 'Form 0000';
		$a = 'select * from form_0000';
		$data['query'] = $this->db->query($a)->result_array();
		view('form_0000',$data);
	}

	public function view_add()
	{
		$data['title'] = 'Tambah Data';
		$a = 'select * from ref_kabupatenkota';
		$data['dati'] = $this->db->query($a)->result_array();
		view3('form_0000','tambah',$data);
	}

	/*public function getdati($sandi='')
	{
		$a = "select * from ref_kabupatenkota where sandi='".$sandi."'";
		$query = $this->db->query($a)->result_array();
		$keterangan = '';
		foreach ($query as $key) {
			$keterangan = $key['keterangan'];
		}
		echo json_encode(array("keterangan"=>$keterangan));
	}
*/
	public function add()
	{
		$periode = $this->input->post('periode');
		$flag_detail = $this->input->post('flag_detail');
		$nama_bpr = $this->input->post('nama_bpr');
		$alamat_bpr = $this->input->post('alamat_bpr');
		$dati_ii_bpr = $this->input->post('dati_ii_bpr');
		$no_telp = $this->input->post('no_telp');
		$npwp = $this->input->post('npwp');
		$pjpl_nama = $this->input->post('pjpl_nama');
		$pjpl_bagian_divisi = $this->input->post('pjpl_bagian_divisi');
		$pjpl_no_telp = $this->input->post('pjpl_no_telp');
		$pjpl_email = $this->input->post('pjpl_email');
		$dividen_nominal = $this->input->post('dividen_nominal');
		$dividen_tahun_rups = $this->input->post('dividen_tahun_rups');
		$bonus_tahunan_tantiem = $this->input->post('bonus_tahunan_tantiem');
		$ialt_nama_kantor = $this->input->post('ialt_nama_kantor');
		$ialt_nama_ap = $this->input->post('ialt_nama_ap');
		$ialt_pemeriksaan = $this->input->post('ialt_pemeriksaan');
		$nilai_nominal = $this->input->post('nilai_nominal');
		$array = array(
			'periode' => $periode, 
			'flag_detail' => $flag_detail,
			'nama_bpr' => $nama_bpr,
			'alamat_bpr' => $alamat_bpr,
			'dati_ii_bpr' => $dati_ii_bpr,
			'no_telp' => $no_telp,
			'npwp' => $npwp,
			'pjpl_nama' => $pjpl_nama,
			'pjpl_bagian_divisi' => $pjpl_bagian_divisi,
			'pjpl_no_telp' => $pjpl_no_telp,
			'pjpl_email' => $pjpl_email,
			'dividen_nominal' => $dividen_nominal,
			'dividen_tahun_rups' => $dividen_tahun_rups,
			'bonus_tahunan_tantiem' => $bonus_tahunan_tantiem,
			'ialt_nama_kantor' => $ialt_nama_kantor,
			'ialt_nama_ap' => $ialt_nama_ap,
			'ialt_pemeriksaan' => $ialt_pemeriksaan,
			'nilai_nominal' => $nilai_nominal
		);
		$this->db->insert('form_0000', $array);
		redirect('form_0000');
	}

	public function view_edit($id)
	{
		$data['title'] = 'Ubah Data';
		$a = 'select * from ref_kabupatenkota';
		$data['dati'] = $this->db->query($a)->result_array();
		$b = "select * from form_0000 where id='".$id."'";
		$data['query'] = $this->db->query($b)->result_array();
		view3('form_0000','ubah',$data);
	}

	public function update()
	{
		$id = $this->input->post('id');
		$periode = $this->input->post('periode');
		$flag_detail = $this->input->post('flag_detail');
		$nama_bpr = $this->input->post('nama_bpr');
		$alamat_bpr = $this->input->post('alamat_bpr');
		$dati_ii_bpr = $this->input->post('dati_ii_bpr');
		$no_telp = $this->input->post('no_telp');
		$npwp = $this->input->post('npwp');
		$pjpl_nama = $this->input->post('pjpl_nama');
		$pjpl_bagian_divisi = $this->input->post('pjpl_bagian_divisi');
		$pjpl_no_telp = $this->input->post('pjpl_no_telp');
		$pjpl_email = $this->input->post('pjpl_email');
		$dividen_nominal = $this->input->post('dividen_nominal');
		$dividen_tahun_rups = $this->input->post('dividen_tahun_rups');
		$bonus_tahunan_tantiem = $this->input->post('bonus_tahunan_tantiem');
		$ialt_nama_kantor = $this->input->post('ialt_nama_kantor');
		$ialt_nama_ap = $this->input->post('ialt_nama_ap');
		$ialt_pemeriksaan = $this->input->post('ialt_pemeriksaan');
		$nilai_nominal = $this->input->post('nilai_nominal');
		$array = array(
			'periode' => $periode, 
			'flag_detail' => $flag_detail,
			'nama_bpr' => $nama_bpr,
			'alamat_bpr' => $alamat_bpr,
			'dati_ii_bpr' => $dati_ii_bpr,
			'no_telp' => $no_telp,
			'npwp' => $npwp,
			'pjpl_nama' => $pjpl_nama,
			'pjpl_bagian_divisi' => $pjpl_bagian_divisi,
			'pjpl_no_telp' => $pjpl_no_telp,
			'pjpl_email' => $pjpl_email,
			'dividen_nominal' => $dividen_nominal,
			'dividen_tahun_rups' => $dividen_tahun_rups,
			'bonus_tahunan_tantiem' => $bonus_tahunan_tantiem,
			'ialt_nama_kantor' => $ialt_nama_kantor,
			'ialt_nama_ap' => $ialt_nama_ap,
			'ialt_pemeriksaan' => $ialt_pemeriksaan,
			'nilai_nominal' => $nilai_nominal
		);
		$this->db->where('id', $id);
		$this->db->update('form_0000', $array);
		redirect('form_0000');
	}

	public function delete()
	{
		$id = $this->input->post('id');
		$this->db->where('id', $id);
		$this->db->delete('form_0000');
	}
}