<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Form_0901 extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$data['title'] = 'Index';
		$data['query'] = $this->db->get('form_0901')->result_array();
		view('form_0901',$data);
	}

	public function view_add()
	{
		$data['title'] = 'Tambah Data';
		view3('form_0901','tambah',$data);
	}

	public function add()
	{
		$periode = $this->input->post('periode');
		$flag_detail = $this->input->post('flag_detail');
		$sandi_kantor = $this->input->post('sandi_kantor');
		$uraian = $this->input->post('uraian');
		$jumlah = $this->input->post('jumlah');
		$array = array(
			'periode' => $periode,
			'flag_detail' => $flag_detail,
			'sandi_kantor' => $sandi_kantor,
			'uraian' => $uraian,
			'jumlah' => $jumlah
		);

		$this->db->insert('form_0901', $array);
		redirect('form_0901');
	}

	public function view_edit($id)
	{
		$data['title'] = 'Ubah Data';
		$where = array('id' => $id);
		$data['query'] = $this->db->get_where('form_0901',$where)->result();
		view3('form_0901','ubah',$data);
	}

	public function update()
	{
		$id = $this->input->post('id');
		$periode = $this->input->post('periode');
		$flag_detail = $this->input->post('flag_detail');
		$sandi_kantor = $this->input->post('sandi_kantor');
		$uraian = $this->input->post('uraian');
		$jumlah = $this->input->post('jumlah');
		$array = array(
			'periode' => $periode,
			'flag_detail' => $flag_detail,
			'sandi_kantor' => $sandi_kantor,
			'uraian' => $uraian,
			'jumlah' => $jumlah
		);
		$this->db->where('id', $id);
		$this->db->update('form_0901', $array);
		redirect('form_0901');
	}

	public function delete()
	{
		$id = $this->input->post('id');
		$this->db->where('id', $id);
		$this->db->delete('form_0901');
	}
}

/* End of file form_0901.php */
/* Location: ./application/controllers/form_0901.php */