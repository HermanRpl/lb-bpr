<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Index extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

	}

	public function index()
	{
		$data['title'] = "Index";
		view('index',$data);
	}
}

/* End of file index.php */
/* Location: ./application/controllers/index.php */
