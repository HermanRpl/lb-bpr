<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-info '">
        <div class="box-header">
          <h2 class="box-title"><b style="font-weight: 1000;font-variant: small-caps;font-size: 30px"> Ubah Data </b></h2>
          <div class="box-tools">
            <div class="margin">
            </div>
          </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <?php foreach ($data_table as $key): ?>

            <form action="<?= base_url('form_0800/edit') ?>" method="POST" enctype="multipart/form-data">
              <div class="form-group col-md-6">
                <label>Periode</label>
                <input type="number" id="periode" name="periode" class="form-control" maxlength="6" value="<?= $key['periode'] ?>">
                <input type="hidden" id="id" name="id" class="form-control" value="<?= $key['id'] ?>">
              </div>

              <div class="form-group col-md-6">
                <label>Flag Detail</label>
                <input type="text" id="flag_detail" name="flag_detail" class="form-control" maxlength="3" value="<?= $key['flag_detail'] ?>">
              </div>

              <div class="form-group col-md-12">
                <label>Sandi Kantor</label>
                <input type="text" id="sandi_kantor" name="sandi_kantor" class="form-control" maxlength="2" value="<?= $key['sandi_kantor'] ?>">
              </div>

              <div class="form-group col-md-6">
                <label>Jenis Aset</label>
                <select name="jenis_aset" id="jenis_aset" class="form-control" onchange="setjenisaset()">
                  <option value=""><?= $key['jenis_aset'] ?></option>
                  <?php foreach ($jenisaset as $a): ?>
                    <option value="<?= $a['sandi']?>"><?= $a['sandi']?>&nbsp&nbsp<?= $a['keterangan']?></option>
                  <?php endforeach ?>
                </select>
              </div>

              <!-- <div class="form-group col-md-6">
                <label>Keterangan</label>
                <input type="text" class="form-control" name="keterangan1" id="keterangan1" readonly>
              </div> -->

              <div class="form-group col-md-6">
                <label>Sumber Perolehan</label>
                <select name="sumber_perolehan" id="sumber_perolehan" class="form-control" onchange="setsumberperolehan()">
                  <option value=""><?= $key['sumber_perolehan'] ?></option>
                  <?php foreach ($sumberperolehan as $b): ?>
                    <option value="<?= $b['sandi']?>"><?= $b['sandi']?>&nbsp&nbsp<?= $b['keterangan']?></option>
                  <?php endforeach ?>
                </select>
              </div>
<!-- 
              <div class="form-group col-md-6">
                <label>Keterangan</label>
                <input type="text" class="form-control" name="keterangan2" id="keterangan2" readonly>
              </div>
 -->
              <div class="form-group col-md-12">
                <label>Status Aset</label>
                <select name="status_aset" id="status_aset" class="form-control" onchange="setstatusaset()">
                  <option value=""><?= $key['status_aset'] ?></option>
                  <?php foreach ($statusaset as $c): ?>
                    <option value="<?= $c['sandi']?>"><?= $c['sandi']?>&nbsp&nbsp<?= $c['keterangan']?></option>
                  <?php endforeach ?>
                </select>
              </div>

              <!-- <div class="form-group col-md-6">
                <label>Keterangan</label>
                <input type="text" class="form-control" name="keterangan3" id="keterangan3" readonly>
              </div> -->

              <div class="form-group col-md-6">
                <label>Biaya Perolehan</label>
                <input type="number" name="biaya_perolehan" id="biaya_perolehan" class="form-control" value="<?= $key['biaya_perolehan'] ?>">
              </div>

              <div class="form-group col-md-6">
                <label>Akumulasi Penyusutan</label>
                <input type="number" name="akumulasi_penyusutan" id="akumulasi_penyusutan" class="form-control" value="<?= $key['akumulasi_penyusutan_amortisasi'] ?>">
              </div>

              <div class="form-group col-md-6">
                <label>Akumulasi Kerugian</label>
                <input type="number" name="akumulasi_kerugian" id="akumulasi_kerugian" class="form-control" value="<?= $key['akumulasi_kerugian_penurunan_nilai'] ?>">
              </div>

              <div class="form-group col-md-6">
                <label>Nilai Tercatat</label>
                <input type="number" name="nilai_tercatat" id="nilai_tercatat" class="form-control" value="<?= $key['nilai_tercatat'] ?>">
              </div>

              <div class="form-group col-md-6">
                <a class="btn btn-warning" href="<?= base_url('form_0800') ?>">Back</a>
                <button type="submit" class="btn btn-primary">Save</button>
              </div>
            </form>
          <?php endforeach ?>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>

<!-- <script>
   function setjenisaset() {
      var sandi = $('#jenis_aset').val();
      $.ajax({
        url: '<?= base_url("form_0800/setData") ?>/' + sandi,
        type: 'POST',
        dataType:"JSON",
        success: function(data){
          $('#keterangan1').val(data.keterangan);
        }
      });
    }

    function setsumberperolehan() {
      var sandi = $('#sumber_perolehan').val();
      $.ajax({
        url: '<?= base_url("form_0800/setperolehan")?>/' + sandi,
        type: 'POST',
        dataType: "JSON",
        success:function(data) {
          $('#keterangan2').val(data.keterangan);
        }
      });
    }

    function setstatusaset() {
      var sandi = $('#status_aset').val();
      $.ajax({
        url: '<?= base_url("form_0800/setstatus")?>/' + sandi,
        type: 'POST',
        dataType: "JSON",
        success:function(data) {
          $('#keterangan3').val(data.keterangan);
        }
      });
    }
</script> -->