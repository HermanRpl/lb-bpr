<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-info '">
        <div class="box-header">
          <h2 class="box-title"><b style="font-weight: 1000;font-variant: small-caps;font-size: 30px"> Form 0800 </b></h2>

          <div class="box-tools">
            <div class="margin">
              <button type="button" class="btn btn-success" data-toggle="modal" data-target="#Tambah">
                <span data-trigger="hover" data-container="body" data-toggle="popover" data-placement="left"  data-original-title="Tambah Data "><i class="fa fa-plus"></i>
                </span>
              </button>

            </div>
          </div>

        </div>

        <!-- /.box-header -->
        <div class="box-body">
          <div class="table-responsive"> 
            <table id="tabel" class="table table-bordered table-hover ">
              <thead >
                <tr class="bg-info">
                  <th  width="100">Opsi</th>
                  <th>No</th>
                  <th>Periode</th>
                  <th>Flag Detail</th>
                  <th>Sandi Kantor</th>
                  <th>Jenis Aset</th>
                  <th>Sumber Perolehan</th>
                  <th>Status Aset</th>
                  <th>Biaya Perolehan</th>
                  <th>Akumulasi Penyusutan</th>
                  <th>Akumulasi Kerugian</th>
                  <th>Nilai Tercatat</th>
                </tr>
              </thead>
              <tbody id="data">
                <?php $no=1;foreach ($query as $key): ?>
                  <tr>
                    <td>
                      <div class="btn-group"> 
                        <a class="btn btn-warning" href="<?= base_url('form_0800/updateform')?>/<?= $key['id'] ?>">
                          <i class="fa fa-edit"></i></a> 
                          <button onclick="hapus(<?= $key['id'] ?>)" type="button" class="btn btn-danger"><i class="fa fa-trash"></i></button>
                        </div>
                      </td>
                      <td><?= $no++ ?></td>
                      <td><?= $key['periode'] ?></td>
                      <td><?= $key['flag_detail'] ?></td>
                      <td><?= $key['sandi_kantor'] ?></td>
                      <td><?= $key['jenis_aset'] ?></td>
                      <td><?= $key['sumber_perolehan'] ?></td>
                      <td><?= $key['status_aset'] ?></td>
                      <td><?= $key['biaya_perolehan'] ?></td>
                      <td><?= $key['akumulasi_penyusutan_amortisasi'] ?></td>
                      <td><?= $key['akumulasi_kerugian_penurunan_nilai'] ?></td>
                      <td><?= $key['nilai_tercatat'] ?></td>

                    </tr>
                  <?php endforeach ?>
                </tbody>
              </table>
            </div>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>

  <div class="modal fade" id="Tambah" tabindex="-1" role="dialog" aria-labelledby="TambahLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="TambahLabel"><strong>Tambah Data</strong></h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body modal-tambah">

          <form id="form-add" method="POST" enctype="multipart/form-data">

            <div class="form-group col-md-6">
              <label>Periode</label>
              <input type="number" id="periode" name="periode" class="form-control">
              <input type="hidden"  id="id" name="id" class="form-control">
            </div>

            <div class="form-group col-md-6">
              <label>Flag Detail</label>
              <input type="text" id="flag_detail" name="flag_detail" class="form-control">
            </div>

            <div class="form-group col-md-12">
              <label>Sandi Kantor</label>
              <input type="text" id="sandi_kantor" name="sandi_kantor" class="form-control">
            </div>

            <div class="form-group col-md-6">
              <label>Jenis Aset</label>
              <select name="jenis_aset" id="jenis_aset" class="form-control" onchange="setjenisaset()">
                <option value="">==Pilih==</option>
                <?php foreach ($jenisaset as $key): ?>
                  <option value="<?= $key['sandi']?>"><?= $key['sandi']?>&nbsp&nbsp<?= $key['keterangan']?></option>
                <?php endforeach ?>
              </select>
            </div>

            <!-- <div class="form-group col-md-6">
              <label>Keterangan</label>
              <input type="text" class="form-control" name="keterangan1" id="keterangan1" readonly>
            </div> -->

            <div class="form-group col-md-6">
              <label>Sumber Perolehan</label>
              <select name="sumber_perolehan" id="sumber_perolehan" class="form-control" onchange="setsumberperolehan()">
                <option value="">==Pilih==</option>
                <?php foreach ($sumberperolehan as $key): ?>
                  <option value="<?= $key['sandi']?>"><?= $key['sandi']?>&nbsp&nbsp<?= $key['keterangan']?></option>
                <?php endforeach ?>
              </select>
            </div>

            <!-- <div class="form-group col-md-6">
              <label>Keterangan</label>
              <input type="text" class="form-control" name="keterangan2" id="keterangan2" readonly>
            </div> -->

            <div class="form-group col-md-12">
              <label>Status Aset</label>
              <select name="status_aset" id="status_aset" class="form-control" onchange="setstatusaset()">
                <option value="">==Pilih==</option>
                <?php foreach ($statusaset as $key): ?>
                  <option value="<?= $key['sandi']?>"><?= $key['sandi']?>&nbsp&nbsp<?= $key['keterangan']?></option>
                <?php endforeach ?>
              </select>
            </div>

           <!--  <div class="form-group col-md-6">
              <label>Keterangan</label>
              <input type="text" class="form-control" name="keterangan3" id="keterangan3" readonly>
            </div> -->

            <div class="form-group col-md-6">
              <label>Biaya Perolehan</label>
              <input type="number" id="biaya_perolehan" name="biaya_perolehan" class="form-control">
            </div>

            <div class="form-group col-md-6">
              <label>Akumulasi Penyusutan</label>
              <input type="number" id="akumulasi_penyusutan" name="akumulasi_penyusutan" class="form-control">
            </div>

            <div class="form-group col-md-6">
              <label>Akumulasi Kerugian</label>
              <input type="number" id="akumulasi_kerugian" name="akumulasi_kerugian" class="form-control">
            </div>

            <div class="form-group col-md-6">
              <label>Nilai Tercatat</label>
              <input type="number" id="nilai_tercatat" name="nilai_tercatat" class="form-control">
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-primary">Save changes</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>

  <script>

    /*function setjenisaset() {
      var sandi = $('#jenis_aset').val();
      $.ajax({
        url: '<?= base_url("form_0800/setData") ?>/' + sandi,
        type: 'POST',
        dataType:"JSON",
        success: function(data){
          $('.modal-tambah #keterangan1').val(data.keterangan);
        }
      });
    }

    function setsumberperolehan() {
      var sandi = $('#sumber_perolehan').val();
      $.ajax({
        url: '<?= base_url("form_0800/setperolehan")?>/' + sandi,
        type: 'POST',
        dataType: "JSON",
        success:function(data) {
          $('.modal-tambah #keterangan2').val(data.keterangan);
        }
      });
    }

    function setstatusaset() {
      var sandi = $('#status_aset').val();
      $.ajax({
        url: '<?= base_url("form_0800/setstatus")?>/' + sandi,
        type: 'POST',
        dataType: "JSON",
        success:function(data) {
          $('.modal-tambah #keterangan3').val(data.keterangan);
        }
      });
    }

    $('#form-add').submit(function(e){
      e.preventDefault();
      var data = $('#form-add').serialize();
      $.ajax({
        url: "<?= base_url('form_0800/save/tambah/') ?>",
        type:"POST",
        data:data,
        success:function(data){
          $('#Tambah').modal('hide');
          read();
        }
      })
    })
*/
    function hapus(id){
      swal({
        title: 'Are you sure?',
        text: "you won't be able to revert this!",
        type: 'warning',
        showCancelButton: true,
            // confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it'
          }).then((result) => {
            if (result.value){
              $.ajax({
                url: '<?= base_url("form_0800/save/hapus")?>/' + id,
                type: "post",
                success:function(){
                  swal({
                    type: "success",
                    title: "successfully",
                    text: "this data has been deleted",
                    showConfirmButton: "true"
                  }).then((result) => {
                    if(result.value){
                      location.reload();
                    }
                  });
                }
              });
            };
          });
        }

      </script>