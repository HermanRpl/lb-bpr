<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-info '">
        <div class="box-header">
          <h2 class="box-title"><b style="font-weight: 1000;font-variant: small-caps;font-size: 30px"> Form 1500 </b></h2>

          <div class="box-tools">
            <div class="margin">
              <a href="<?= base_url('form_1500/view_add') ?>">
                <span data-trigger="hover" data-container="body" data-toggle="popover" data-placement="left"  data-original-title="Tambah Data "><i class="fa fa-plus"></i></a>
                </span>
            </div>
          </div>

        </div>

        <!-- /.box-header -->
        <div class="box-body">
          <div class="table-responsive"> 
            <table id="tabel" class="table table-bordered table-hover ">
              <thead >
                <tr class="bg-danger">
                  <th width="100" rowspan="2">Opsi</th>
                  <th rowspan="2">No</th>
                  <th rowspan="2">Periode</th>
                  <th rowspan="2">Flag Detail</th>
                  <th rowspan="2">Sandi Kantor</th>
                  <th rowspan="2">Nomor Cif</th>
                  <th rowspan="2">No Rekening</th>
                  <th rowspan="2">Jenis</th>
                  <th rowspan="2">Gol Debitur</th>
                  <th rowspan="2">Hubungan Bank</th>
                  <th rowspan="2">Tgl Hapus Buku</th>
                  <th colspan="3"><center>Saldo Pokok</center></th>
                  <th colspan="4"><center>Tunggakan Bunga</center></th>
                  <th colspan="3"><center>Agunan</center></th>
                </tr>
                <tr class="bg-danger">
                  <th>Hapus Buku</th>
                  <th>Akumulasi Tertagih</th>
                  <th>Per Posisi Laporan</th>
                  <th>Hapus Buku</th>
                  <th>Akumulasi Tertagih</th>
                  <th>Akumulasi Bunga Berjalan</th>
                  <th>Per Posisi Laporan</th>
                  <th>Jenis</th>
                  <th>Alamat</th>
                  <th>Nilai</th>
                </tr>
              </thead>
              <tbody id="data">
                <?php $no=1;foreach ($query as $key): ?>
                  <tr>
                    <td>
                      <div class="btn-group"> 
                        <a class="btn btn-warning" href="<?= base_url('form_1500/view_edit')?>/<?= $key['id'] ?>">
                          <i class="fa fa-edit"></i></a> 
                          <button onclick="hapus(<?= $key['id'] ?>)" type="button" class="btn btn-danger"><i class="fa fa-trash"></i></button>
                        </div>
                      </td>
                      <td><?= $no++ ?></td>
                      <td><?= $key['periode'] ?></td>
                      <td><?= $key['flag_detail'] ?></td>
                      <td><?= $key['sandi_kantor'] ?></td>
                      <td><?= $key['nomor_cif'] ?></td>
                      <td><?= $key['no_rekening'] ?></td>
                      <td><?= $key['jenis'] ?></td>
                      <td><?= $key['gol_debitur'] ?></td>
                      <td><?= $key['hubungan_bank'] ?></td>
                      <td><?= $key['tgl_hapus_buku'] ?></td>
                      <td><?= $key['sp_hapus_buku'] ?></td>
                      <td><?= $key['sp_akumulasi_tertagih'] ?></td>
                      <td><?= $key['sp_per_posisi_laporan'] ?></td>
                      <td><?= $key['tb_hapus_buku'] ?></td>
                      <td><?= $key['tb_akumulasi_tertagih'] ?></td>
                      <td><?= $key['tb_akumulasi_bunga_berjalan'] ?></td>
                      <td><?= $key['tb_per_posisi_laporan'] ?></td>
                      <td><?= $key['agunan_jenis'] ?></td>
                      <td><?= $key['agunan_alamat'] ?></td>
                      <td><?= $key['agunan_nilai'] ?></td>
                    </tr>
                  <?php endforeach ?>
                </tbody>
              </table>
            </div>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
<script>
  function hapus(id){
      swal({
        title: 'Are you sure?',
        text: "you won't be able to revert this!",
        type: 'warning',
        showCancelButton: true,
            // confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it'
          }).then((result) => {
            if (result.value){
              $.ajax({
                url: '<?= base_url("form_1500/delete")?>',
                type: "post",
                data: {id,id},
                success:function(){
                  swal({
                    type: "success",
                    title: "successfully",
                    text: "this data has been deleted",
                    showConfirmButton: "true"
                  }).then((result) => {
                    if(result.value){
                      location.reload();
                    }
                  });
                }
              });
            };
          });
        }
</script>