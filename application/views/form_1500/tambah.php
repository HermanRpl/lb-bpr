<!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-info '">
				<div class="box-header">
					<h2 class="box-title"><b style="font-weight: 1000;font-variant: small-caps;font-size: 30px"> Tambah Data </b></h2>
					<div class="box-tools">
						<div class="margin">
						</div>
					</div>
				</div>
				<!-- /.box-header -->
				<div class="box-body" >

					<form action="<?= base_url('form_1500/add')?>" method="POST" enctype="multipart/form-data">

						<div class="form-group col-md-6">
							<label>Periode</label>
							<input type="number" id="periode" name="periode" class="form-control">
						</div>

						<div class="form-group col-md-6">
							<label>Flag Detail</label>
							<input type="text" id="flag_detail" name="flag_detail" class="form-control">
						</div>

						<div class="form-group col-md-6">
							<label>Sandi Kantor</label>
							<input type="text" id="sandi_kantor" name="sandi_kantor" class="form-control">
						</div>

						<div class="form-group col-md-6">
							<label>Nomor CIf</label>
							<input type="text" id="nomor_cif" name="nomor_cif" class="form-control">
						</div>

						<div class="form-group col-md-12">
							<label>No Rekening</label>
							<input type="text" id="no_rekening" name="no_rekening" class="form-control">
						</div>
						
						<div class="form-group col-md-6">
							<label>Jenis</label>
							<select name="jenis" id="jenis" class="form-control" onchange="setjenisnya()">
								<option value="">==Pilih==</option>
								<?php foreach ($jenisnya as $key): ?>
									<option value="<?= $key['sandi']?>"><?= $key['sandi']?>&nbsp&nbsp<?= $key['keterangan']?></option>
								<?php endforeach ?>
							</select>
						</div>

						<div class="form-group col-md-6">
							<label>Gol Debitur</label>
							<select name="gol_debitur" id="gol_debitur" class="form-control" onchange="setgoldebitur()">
								<option value="">==Pilih</option>
								<?php foreach ($goldebitur as $key): ?>
									<option value="<?= $key['sandi']?>"><?= $key['sandi']?>&nbsp&nbsp<?= $key['keterangan']?></option>
								<?php endforeach ?>
							</select>
						</div>

						<div class="form-group col-md-6">
							<label>Hubungan Bank</label>
							<select name="hubungan_bank" id="hubungan_bank" class="form-control" onchange="sethubunganbank()">
								<option value="">==Pilih</option>
								<?php foreach ($hubunganbank as $key): ?>
									<option value="<?= $key['sandi']?>"><?= $key['sandi']?>&nbsp&nbsp<?= $key['keterangan']?></option>
								<?php endforeach ?>
							</select>
						</div>

						<div class="form-group col-md-6">
							<label>Tgl Hapus Buku</label>
							<input type="date" id="tgl_hapus_buku" name="tgl_hapus_buku" class="form-control">
						</div>

						<div class="form-group col-md-6">
							<label>Sp Hapus buku</label>
							<input type="number" id="sp_hapus_buku" name="sp_hapus_buku" class="form-control">
						</div>

						<div class="form-group col-md-6">
							<label>Sp Akumulasi Tertagih</label>
							<input type="number" id="sp_akumulasi_tertagih" name="sp_akumulasi_tertagih" class="form-control">
						</div>

						<div class="form-group col-md-6">
							<label>Sp Per posisi Laporan</label>
							<input type="number" id="sp_per_posisi_laporan" name="sp_per_posisi_laporan" class="form-control">
						</div>

						<div class="form-group col-md-6">
							<label>Tb Hapus Buku</label>
							<input type="number" id="tb_hapus_buku" name="tb_hapus_buku" class="form-control">
						</div>

						<div class="form-group col-md-6">
							<label>Tb Akumulasi Tertagih</label>
							<input type="number" id="tb_akumulasi_tertagih" name="tb_akumulasi_tertagih" class="form-control">
						</div>

						<div class="form-group col-md-6">
							<label>Tb Akumulasi Bunga Berjalan</label>
							<input type="number" id="tb_akumulasi_bunga_berjalan" name="tb_akumulasi_bunga_berjalan" class="form-control">
						</div>

						<div class="form-group col-md-6">
							<label>Tb Per posisi Laporan</label>
							<input type="number" id="tb_per_posisi_laporan" name="tb_per_posisi_laporan" class="form-control">
						</div>

						<div class="form-group col-md-6">
							<label>Agunan Jenis</label>
							<select name="agunan_jenis" id="agunan_jenis" onchange="setagunanjenis()" class="form-control">
								<option value="">==Pilih==</option>
								<?php foreach ($agunanjenis as $key): ?>
									<option value="<?= $key['sandi']?>"><?= $key['sandi']?>&nbsp&nbsp<?= $key['keterangan']?></option>
								<?php endforeach ?>
							</select>
						</div>

						<div class="form-group col-md-12">
							<label>Agunan Alamat</label>
							<input type="text" id="agunan_alamat" name="agunan_alamat" class="form-control">
						</div>

						<div class="form-group col-md-12">
							<label>Agunan Nilai</label>
							<input type="number" id="agunan_nilai" name="agunan_nilai" class="form-control">
						</div>

						<div class="form-group col-md-12">
							<a class="btn btn-warning" href="<?= base_url('form_1500') ?>">Back</a>
							<button type="submit" class="btn btn-primary">Save</button>
						</div>

					</form>

				</div>
				<!-- /.box-body -->
			</div>
			<!-- /.box -->
		</div>
		<!-- /.col -->
	</div>
	<!-- /.row -->
</section>
<!-- <script>
	function setjenisnya() {
		var sandi = $('#jenis').val()
		$.ajax({
			url: '<?= base_url("form_1500/setjenis") ?>/' + sandi,
			type: 'POST',
			dataType: 'JSON',
			success:function(data) {
				$('#keterangan').val(data.keterangan);
			}
		})
	}

	function setgoldebitur() {
		var sandi = $('#gol_debitur').val()
		$.ajax({
			url: '<?= base_url("form_1500/setgol") ?>/' + sandi,
			type: 'POST',
			dataType: 'JSON',
			success:function(data) {
				$('#keterangan1').val(data.keterangan);
			}
		})
	}

	function sethubunganbank() {
		var sandi = $('#hubungan_bank').val()
		$.ajax({
			url: '<?= base_url("form_1500/sethubungan") ?>/' + sandi,
			type: 'POST',
			dataType: 'JSON',
			success:function(data) {
				$('#keterangan2').val(data.keterangan);
			}
		})
	}

	function setagunanjenis() {
		var sandi = $('#agunan_jenis').val()
		$.ajax({
			url: '<?= base_url("form_1500/setagunan") ?>/' + sandi,
			type: 'POST',
			dataType: 'JSON',
			success:function(data) {
				$('#keterangan3').val(data.keterangan);
			}
		})
	}
</script> -->