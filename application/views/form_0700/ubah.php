<!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-info '">
				<div class="box-header">
					<h2 class="box-title"><b style="font-weight: 1000;font-variant: small-caps;font-size: 30px"> Ubah Data </b></h2>
					<div class="box-tools">
						<div class="margin">
						</div>
					</div>
				</div>
				<!-- /.box-header -->
				<div class="box-body" >
					<?php foreach ($datanya as $row): ?>
						
						<form action="<?= base_url('form_0700/update')?>" method="POST" enctype="multipart/form-data">

							<div class="form-group col-md-6">
								<label>Periode</label>
								<input type="number" id="periode" name="periode" class="form-control" value="<?= $row['periode']?>">
								<input type="hidden" id="id" name="id" class="form-control" value="<?= $row['id']?>">
							</div>

							<div class="form-group col-md-6">
								<label>Flag Detail</label>
								<input type="text" id="flag_detail" name="flag_detail" class="form-control" value="<?= $row['flag_detail']?>">
							</div>

							<div class="form-group col-md-12">
								<label>Sandi Kantor</label>
								<input type="text" id="sandi_kantor" name="sandi_kantor" class="form-control" value="<?= $row['sandi_kantor']?>">
							</div>

							<div class="form-group col-md-12">
								<label>Jenis Agunan</label>
								<select name="jenis_agunan" id="jenis_agunan" class="form-control">
									<option value="<?= $row['jenis_agunan']?>"><?= $row['jenis_agunan']?></option>
									<?php foreach ($jenisagunan as $key): ?>
										<option value="<?= $key['sandi']?>"><?= $key['sandi']?> - <?= $key['keterangan']?></option>
									<?php endforeach ?>
								</select>
							</div>

							<div class="form-group col-md-6">
								<label>Alamat Agunan</label>
								<input type="text" id="alamat_agunan" name="alamat_agunan" class="form-control" value="<?= $row['alamat_agunan']?>">
							</div>

							<div class="form-group col-md-6">
								<label>Tgl Pengambil Alihan</label>
								<input type="date" id="tgl_pengambilalihan" name="tgl_pengambilalihan" class="form-control" value="<?= $row['tgl_pengambilalihan']?>">
							</div>

							<div class="form-group col-md-6">
								<label>Nilai Pengakuan Awal</label>
								<input type="number" id="nilai_pengakuan_awal" name="nilai_pengakuan_awal" class="form-control" value="<?= $row['nilai_pengakuan_awal']?>">
							</div>

							<div class="form-group col-md-6">
								<label>Akumulasi Kerugian Penurunan</label>
								<input type="number" id="akumulasi_kerugian_penurunan" name="akumulasi_kerugian_penurunan" class="form-control" value="<?= $row['nilai_pengakuan_awal']?>">
							</div>

							<div class="form-group col-md-12">
								<label>Jumlah</label>
								<input type="number" id="jumlah" name="jumlah" class="form-control" value="<?= $row['jumlah']?>">
							</div>

							<div class="form-group col-md-12">
								<a class="btn btn-warning" href="<?= base_url('form_0700') ?>">Back</a>
								<button type="submit" class="btn btn-primary">Save</button>
							</div>

						</form>
					<?php endforeach ?>
				</div>
				<!-- /.box-body -->
			</div>
			<!-- /.box -->
		</div>
		<!-- /.col -->
	</div>
	<!-- /.row -->
</section>
