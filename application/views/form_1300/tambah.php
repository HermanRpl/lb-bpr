<!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-info '">
				<div class="box-header">
					<h2 class="box-title"><b style="font-weight: 1000;font-variant: small-caps;font-size: 30px"> Tambah Data </b></h2>
					<div class="box-tools">
						<div class="margin">
						</div>
					</div>
				</div>
				<!-- /.box-header -->
				<div class="box-body" >

					<form action="<?= base_url('form_1300/add')?>" method="POST" enctype="multipart/form-data">

						<div class="form-group col-md-6">
							<label>Periode</label>
							<input type="number" id="periode" name="periode" class="form-control">
						</div>

						<div class="form-group col-md-6">
							<label>Flag Detail</label>
							<input type="text" id="flag_detail" name="flag_detail" class="form-control">
						</div>

						<div class="form-group col-md-6">
							<label>Sandi Kantor</label>
							<input type="text" id="sandi_kantor" name="sandi_kantor" class="form-control">
						</div>

						<div class="form-group col-md-6">
							<label>Nomor Cif</label>
							<input type="text" id="nomor_cif" name="nomor_cif" class="form-control">
						</div>

						<div class="form-group col-md-12">
							<label>Nomor rekening</label>
							<input type="text" id="nomor_rekening" name="nomor_rekening" class="form-control">
						</div>

						<div class="form-group col-md-6">
							<label>Jenis Bank</label>
							<select name="jenis_bank" id="jenis_bank" class="form-control" onchange="setjenisbank()">
								<option value="">==Pilih==</option>
								<?php foreach ($jeba as $key): ?>
									<option value="<?= $key['sandi']?>"><?= $key['sandi']?>&nbsp&nbsp<?= $key['keterangan']?></option>	
								<?php endforeach ?>
							</select>
						</div>

						<div class="form-group col-md-6">
							<label>Sandi Bank</label>
							<select name="sandi_bank" id="sandi_bank" class="form-control" onchange="setsandibank()">
								<option value="">==Pilih==</option>
								<?php foreach ($saba as $key): ?>
									<option value="<?= $key['sandi']?>"><?= $key['sandi']?>&nbsp&nbsp<?= $key['keterangan']?></option>	
								<?php endforeach ?>
							</select>
						</div>

						<div class="form-group col-md-6">
							<label>Lokasi Bank</label>
							<select name="lokasi_bank" id="lokasi_bank" class="form-control" onchange="setlokasibank()">
								<option value="">==Pilih==</option>
								<?php foreach ($loba as $key): ?>
									<option value="<?= $key['sandi']?>"><?= $key['sandi']?>&nbsp&nbsp<?= $key['keterangan']?></option>	
								<?php endforeach ?>
							</select>
						</div>

						<div class="form-group col-md-6">
							<label>Jenis</label>
							<select name="jenis" id="jenis" class="form-control" onchange="setjenis()">
								<option value="">==Pilih==</option>
								<?php foreach ($jeniss as $key): ?>
									<option value="<?= $key['sandi']?>"><?= $key['sandi']?>&nbsp&nbsp<?= $key['keterangan']?></option>	
								<?php endforeach ?>
							</select>
						</div>

						<div class="form-group col-md-6">
							<label>Hubungan Bank</label>
							<select name="hubungan_bank" id="hubungan_bank" class="form-control" onchange="sethubunganbank()">
								<option value="">==Pilih==</option>
								<?php foreach ($huba as $key): ?>
									<option value="<?= $key['sandi']?>"><?= $key['sandi']?>&nbsp&nbsp<?= $key['keterangan']?></option>	
								<?php endforeach ?>
							</select>
						</div>

						<div class="form-group col-md-6">
							<label>Tgl Mulai</label>
							<input type="date" name="tgl_mulai" id="tgl_mulai" class="form-control">
						</div>

						<div class="form-group col-md-6">
							<label>Tgl Jatuh Tempo</label>
							<input type="date" name="tgl_jatuh_tempo" id="tgl_jatuh_tempo" class="form-control">
						</div>

						<div class="form-group col-md-6">
							<label>Suku Bunga</label>
							<input type="number" name="suku_bunga" id="suku_bunga" class="form-control">
						</div>

						<div class="form-group col-md-6">
							<label>Nominal</label>
							<input type="number" name="nominal" id="nominal" class="form-control">
						</div>

						<div class="form-group col-md-6">
							<label>Nominal Diblokir</label>
							<input type="number" name="nominal_diblokir" id="nominal_diblokir" class="form-control">
						</div>

						<div class="form-group col-md-6">
							<label>Alasan Diblokir</label>
							<select name="alasan_diblokir" id="alasan_diblokir" class="form-control" onchange="setalasandiblokir()">
								<option value="">==Pilih==</option>
								<?php foreach ($alasan as $key): ?>
									<option value="<?= $key['sandi']?>"><?= $key['sandi']?>&nbsp&nbsp<?= $key['keterangan']?></option>	
								<?php endforeach ?>
							</select>
						</div>

						<div class="form-group col-md-6">
							<label>Transaksi Belum Diamortisasi</label>
							<input type="number" name="transaksi_belum_diamortisasi" id="transaksi_belum_diamortisasi" class="form-control">
						</div>

						<div class="form-group col-md-12">
							<label>Jumlah</label>
							<input type="number" name="jumlah" id="jumlah" class="form-control">
						</div>

						<div class="form-group col-md-12">
							<a class="btn btn-warning" href="<?= base_url('form_1300') ?>">Back</a>
							<button type="submit" class="btn btn-primary">Save</button>
						</div>

					</form>

				</div>
				<!-- /.box-body -->
			</div>
			<!-- /.box -->
		</div>
		<!-- /.col -->
	</div>
	<!-- /.row -->
</section>
<script>
	function setjenisbank() {
		var sandi = $('#jenis_bank').val();
		$.ajax({
			url: '<?= base_url("form_1300/setjeba") ?>/' + sandi,
			type: 'POST',
			dataType: 'JSON',
			success:function(data) {
				$('#keterangan').val(data.keterangan);
			}
		})
	}

	function setsandibank() {
		var sandi = $('#sandi_bank').val();
		$.ajax({
			url: '<?= base_url("form_1300/setsandibankk") ?>/' + sandi,
			type: 'POST',
			dataType: 'JSON',
			success:function(data) {
				$('#keterangan1').val(data.keterangan);
			}
		})
	}

	function setlokasibank() {
		var sandi = $('#lokasi_bank').val();
		$.ajax({
			url: '<?= base_url("form_1300/setlokasibankk") ?>/' + sandi,
			type: 'POST',
			dataType: 'JSON',
			success:function(data) {
				$('#keterangan2').val(data.keterangan);
			}
		})
	}

	function setjenis() {
		var sandi = $('#jenis').val();
		$.ajax({
			url: '<?= base_url("form_1300/setjenisss") ?>/' + sandi,
			type: 'POST',
			dataType: 'JSON',
			success:function(data) {
				$('#keterangan3').val(data.keterangan);
			}
		})
	}

	function sethubunganbank() {
		var sandi = $('#hubungan_bank').val();
		$.ajax({
			url: '<?= base_url("form_1300/sethubunganbankk") ?>/' + sandi,
			type: 'POST',
			dataType: 'JSON',
			success:function(data) {
				$('#keterangan4').val(data.keterangan);
			}
		})
	}

	function setalasandiblokir() {
		var sandi = $('#alasan_diblokir').val();
		$.ajax({
			url: '<?= base_url("form_1300/setalasannya") ?>/' + sandi,
			type: 'POST',
			dataType: 'JSON',
			success:function(data) {
				$('#keterangan5').val(data.keterangan);
			}
		})
	}
</script>