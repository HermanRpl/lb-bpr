<!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-info '">
				<div class="box-header">
					<h2 class="box-title"><b style="font-weight: 1000;font-variant: small-caps;font-size: 30px"> Tambah Data </b></h2>
					<div class="box-tools">
						<div class="margin">
						</div>
					</div>
				</div>
				<!-- /.box-header -->
				<div class="box-body" >

					<form action="<?= base_url('form_1200/add')?>" method="POST" enctype="multipart/form-data">

						<div class="form-group col-md-6">
							<label>Periode</label>
							<input type="number" id="periode" name="periode" class="form-control">
						</div>

						<div class="form-group col-md-6">
							<label>Flag Detail</label>
							<input type="text" id="flag_detail" name="flag_detail" class="form-control">
						</div>

						<div class="form-group col-md-6">
							<label>Sandi Kantor</label>
							<input type="text" id="sandi_kantor" name="sandi_kantor" class="form-control">
						</div>

						<div class="form-group col-md-6">
							<label>Nomor Cif</label>
							<input type="text" id="nomor_cif" name="nomor_cif" class="form-control">
						</div>

						<div class="form-group col-md-12">
							<label>Nomor rekening</label>
							<input type="text" id="nomor_rekening" name="nomor_rekening" class="form-control">
						</div>

						<div class="form-group col-md-6">
							<label>Hubungan Bank</label>
							<select name="hubungan_bank" id="hubungan_bank" class="form-control" onchange="sethubunganbank()">
								<option value="">==Pilih==</option>
								<?php foreach ($hubung as $key): ?>
									<option value="<?= $key['sandi']?>"><?= $key['sandi']?>&nbsp&nbsp<?= $key['keterangan']?></option>
								<?php endforeach ?>
							</select>
						</div>

						<div class="form-group col-md-6">
							<label>Golongan Nasabah</label>
							<select name="golongan_nasabah" id="golongan_nasabah" class="form-control" onchange="setgolongannasabah()">
								<option value="">==Pilih==</option>
								<?php foreach ($golong as $key): ?>
									<option value="<?= $key['sandi']?>"><?= $key['sandi']?>&nbsp&nbsp<?= $key['keterangan']?></option>
								<?php endforeach ?>
							</select>
						</div>

						<div class="form-group col-md-6">
							<label>Lokasi Nasabah</label>
							<select name="lokasi_nasabah" id="lokasi_nasabah" class="form-control" onchange="setlokasinasabah()">
								<option value="">==Pilih==</option>
								<?php foreach ($lok as $key): ?>
									<option value="<?= $key['sandi']?>"><?= $key['sandi']?>&nbsp&nbsp<?= $key['keterangan']?></option>
								<?php endforeach ?>
							</select>
						</div>


						<div class="form-group col-md-6">
							<label>Tanggal Mulai</label>
							<input type="date" id="jw_mulai" name="jw_mulai" class="form-control">
						</div>

						<div class="form-group col-md-6">
							<label>Jatuh Tempo</label>
							<input type="date" id="jw_jatuh_tempo" name="jw_jatuh_tempo" class="form-control">
						</div>

						<div class="form-group col-md-6">
							<label>Suku Bunga</label>
							<input type="number" id="suku_bunga" name="suku_bunga" class="form-control">
						</div>

						<div class="form-group col-md-6">
							<label>Nominal</label>
							<input type="number" id="nominal" name="nominal" class="form-control">
						</div>

						<div class="form-group col-md-6">
							<label>Nominal Diblokir</label>
							<input type="number" id="nominal_diblokir" name="nominal_diblokir" class="form-control">
						</div>

						<div class="form-group col-md-12">
							<label>Alasan Diblokir</label>
							<select name="alasan_diblokir" id="alasan_diblokir" class="form-control" onchange="setalasan()">
								<option value="">==Pilih==</option>
								<?php foreach ($alasan as $key): ?>
									<option value="<?= $key['sandi']?>"><?= $key['sandi']?>&nbsp&nbsp<?= $key['keterangan']?></option>
								<?php endforeach ?>
							</select>
						</div>

						<div class="form-group col-md-6">
							<label>Biaya Transaksi</label>
							<input type="number" id="biaya_transaksi" name="biaya_transaksi" class="form-control">
						</div>

						<div class="form-group col-md-6">
							<label>Jumlah</label>
							<input type="number" id="jumlah" name="jumlah" class="form-control">
						</div>

						<div class="form-group col-md-12">
							<a class="btn btn-warning" href="<?= base_url('form_1200') ?>">Back</a>
							<button type="submit" class="btn btn-primary">Save</button>
						</div>
					</form>

				</div>
				<!-- /.box-body -->
			</div>
			<!-- /.box -->
		</div>
		<!-- /.col -->
	</div>
	<!-- /.row -->
</section>
<!-- <script>

	function sethubunganbank() {
		var sandi = $('#hubungan_bank').val();
		$.ajax({
			url: '<?= base_url("form_1200/sethubungan") ?>/' + sandi,
			type: 'POST',
			dataType: 'JSON',
			success:function(data) {
				$('#keterangan2').val(data.keterangan);
			}
		})
	}

	function setgolongannasabah() {
		var sandi = $('#golongan_nasabah').val();
		$.ajax({
			url: '<?= base_url("form_1200/setgolongan") ?>/' + sandi,
			type: 'POST',
			dataType: 'JSON',
			success:function(data) {
				$('#keterangan3').val(data.keterangan);
			}
		})
	}

	function setlokasinasabah() {
		var sandi = $('#lokasi_nasabah').val();
		$.ajax({
			url: '<?= base_url("form_1200/setlokasi") ?>/' + sandi,
			type: 'POST',
			dataType: 'JSON',
			success:function(data) {
				$('#keterangan4').val(data.keterangan);
			}
		})
	}

	function setalasan() {
		var sandi = $('#alasan_diblokir').val();
		$.ajax({
			url: '<?= base_url("form_1200/setalas") ?>/' + sandi,
			type: 'POST',
			dataType: 'JSON',
			success:function(data) {
				$('#keterangan5').val(data.keterangan);
			}
		})
	}
</script> -->