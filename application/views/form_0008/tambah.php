<!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-info '">
				<div class="box-header">
					<h2 class="box-title"><b style="font-weight: 1000;font-variant: small-caps;font-size: 30px"> Tambah Data </b></h2>
					<div class="box-tools">
						<div class="margin">
						</div>
					</div>
				</div>
				<!-- /.box-header -->
				<div class="box-body" >

					<form action="<?= base_url('form_0008/add')?>" method="POST" enctype="multipart/form-data">

						<div class="form-group col-md-12">
							<label>Periode</label>
							<input type="number" id="periode" name="periode" class="form-control">
						</div>

						<div class="form-group col-md-12">
							<label>Flag Detail</label>
							<input type="text" id="flag_detail" name="flag_detail" class="form-control">
						</div>

						<div class="form-group col-md-12">
							<label>Sandi Pos</label>
							<select name="sandi_pos" id="sandi_pos" style="width: 100%" class="form-control" onchange="setsandipos()">
								<option value="">==Pilih==</option>
								<?php foreach ($sandii as $key): ?>
								<option value="<?= $key['sandi']?>"><?= $key['sandi']?> - <?= $key['keterangan']?></option>
								<?php endforeach ?>
							</select>
						</div>

						<div class="form-group col-md-12">
							<label>Nilai Rasio</label>
							<input type="number" id="nilai_rasio" name="nilai_rasio" class="form-control">
						</div>

						<div class="form-group col-md-12">
							<a class="btn btn-warning" href="<?= base_url('form_0008') ?>">Back</a>
							<button type="submit" class="btn btn-primary">Save</button>
						</div>

					</form>

				</div>
				<!-- /.box-body -->
			</div>
			<!-- /.box -->
		</div>
		<!-- /.col -->
	</div>
	<!-- /.row -->
</section>