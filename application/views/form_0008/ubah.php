<!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-info '">
				<div class="box-header">
					<h2 class="box-title"><b style="font-weight: 1000;font-variant: small-caps;font-size: 30px"> Ubah Data </b></h2>
					<div class="box-tools">
						<div class="margin">
						</div>
					</div>
				</div>
				<!-- /.box-header -->
				<div class="box-body" >
					<?php foreach ($dataku as $row): ?>
						
						<form action="<?= base_url('form_0008/update')?>" method="POST" enctype="multipart/form-data">

							<div class="form-group col-md-12">
								<label>Periode</label>
								<input type="number" id="periode" name="periode" class="form-control" value="<?= $row['periode'] ?>">
								<input type="hidden" id="id" name="id" class="form-control" value="<?= $row['id'] ?>">
							</div>

							<div class="form-group col-md-12">
								<label>Flag Detail</label>
								<input type="text" id="flag_detail" name="flag_detail" class="form-control" value="<?= $row['flag_detail'] ?>">
							</div>

							<div class="form-group col-md-12">
								<label>Sandi Pos</label>
								<select name="sandi_pos" id="sandi_pos" class="form-control" onchange="setsandipos()">
									<option value="<?= $row['sandi_pos'] ?>"><?= $row['sandi_pos'] ?></option>
									<?php foreach ($sandii as $key): ?>
										<option value="<?= $key['sandi']?>"><?= $key['sandi']?> - <?= $key['keterangan']?></option>
									<?php endforeach ?>
								</select>
							</div>

							<div class="form-group col-md-12">
								<label>Nilai Rasio</label>
								<input type="number" id="nilai_rasio" name="nilai_rasio" class="form-control" value="<?= $row['nilai_rasio'] ?>">
							</div>

							<div class="form-group col-md-12">
								<a class="btn btn-warning" href="<?= base_url('form_0008') ?>">Back</a>
								<button type="submit" class="btn btn-primary">Save</button>
							</div>

						</form>
					<?php endforeach ?>
				</div>
				<!-- /.box-body -->
			</div>
			<!-- /.box -->
		</div>
		<!-- /.col -->
	</div>
	<!-- /.row -->
</section>
<script>
	function setsandipos() {
		var sandi = $('#sandi_pos').val();
		$.ajax({
			url: '<?= base_url("form_0008/setsandi") ?>/' + sandi,
			type: 'POST',
			dataType: 'JSON',
			success:function(data) {
				$('#keterangan').val(data.keterangan);
			}
		});
	}
</script>