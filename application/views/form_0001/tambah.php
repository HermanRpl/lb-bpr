<!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-info '">
				<div class="box-header">
					<h2 class="box-title"><b style="font-weight: 1000;font-variant: small-caps;font-size: 30px"> Tambah Data </b></h2>
					<div class="box-tools">
						<div class="margin">
						</div>
					</div>
				</div>
				<!-- /.box-header -->
				<div class="box-body" >

					<form action="<?= base_url('form_0001/add')?>" method="POST" enctype="multipart/form-data">

						<div class="form-group col-md-6">
							<label>Periode</label>
							<input type="number" id="periode" name="periode" class="form-control">
						</div>

						<div class="form-group col-md-6">
							<label>Flag Detail</label>
							<input type="text" id="flag_detail" name="flag_detail" class="form-control">
						</div>

						<div class="form-group col-md-6">
							<label>Nama</label>
							<input type="text" id="nama" name="nama" class="form-control">
						</div>

						<div class="form-group col-md-6">
							<label>Alamat</label>
							<input type="text" id="alamat" name="alamat" class="form-control">
						</div>

						<div class="form-group col-md-12">
							<label>Jenis</label>
							<select class="form-control" name="jenis" id="jenis">
								<option value="">==Pilih==</option>
								<?php foreach ($jenis as $key): ?>
									<option value="<?= $key['sandi']?>"><?= $key['sandi']?>&nbsp&nbsp<?= $key['keterangan']?></option>
								<?php endforeach ?>
							</select>
						</div>

						<div class="form-group col-md-12">
							<label>No Identitas</label>
							<input type="text" id="no_identitas" name="no_identitas" class="form-control">
						</div>

						<div class="form-group col-md-12">
							<label>PSP</label>
							<select class="form-control" name="psp" id="psp">
								<option value="1">Ya</option>
								<option value="2">Tidak</option>
							</select>
						</div>

						<div class="form-group col-md-6">
							<label>Jumlah Nominal</label>
							<input type="number" id="jumlah_nominal" name="jumlah_nominal" class="form-control">
						</div>

						<div class="form-group col-md-6">
							<label>Persentase Kepemilikan</label>
							<input type="number" id="persentase_kepemilikan" name="persentase_kepemilikan" class="form-control">
						</div>

						<div class="form-group col-md-12">
							<a class="btn btn-warning" href="<?= base_url('form_0001') ?>">Back</a>
							<button type="submit" class="btn btn-primary">Save</button>
						</div>

					</form>

				</div>
				<!-- /.box-body -->
			</div>
			<!-- /.box -->
		</div>
		<!-- /.col -->
	</div>
	<!-- /.row -->
</section>