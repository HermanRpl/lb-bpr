<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-info '">
        <div class="box-header">
          <h2 class="box-title"><b style="font-weight: 1000;font-variant: small-caps;font-size: 30px"> Form 1000 </b></h2>

          <div class="box-tools">
            <div class="margin">
              <a href="<?= base_url('form_1000/view_add') ?>">
                <span data-trigger="hover" data-container="body" data-toggle="popover" data-placement="left"  data-original-title="Tambah Data "><i class="fa fa-plus"></i>
                </span>
              </a>

            </div>
          </div>

        </div>

        <!-- /.box-header -->
        <div class="box-body">
          <div class="table-responsive"> 
            <table id="tabel" class="table table-bordered table-hover ">
              <thead >
                <tr class="bg-info">
                  <th  width="100">Opsi</th>
                  <th>No</th>
                  <th>Periode</th>
                  <th>Flag Detail</th>
                  <th>Sandi Kantor</th>
                  <th>Sandi Pos</th>
                  <th>Jumlah</th>
                </tr>
              </thead>
              <tbody id="data">
                <?php $no=1; foreach ($query as $key): ?>

                  <tr>
                    <td>
                      <div class="btn-group"> 
                        <a class="btn btn-primary" href="<?= base_url('form_1000/view_edit')?>/<?= $key['id'] ?>"><i class="fa fa-edit"></i></a>
                        <button onclick="hapus(<?= $key['id'] ?>)" type="button" class="btn btn-danger"><i class="fa fa-trash"></i></button>
                      </div>
                    </td>
                    <td><?= $no++ ?></td>
                    <td><?= $key['periode'] ?></td>
                    <td><?= $key['flag_detail'] ?></td>
                    <td><?= $key['sandi_kantor'] ?></td>
                    <td><?= $key['sandi_pos'] ?></td>
                    <td><?= $key['jumlah'] ?></td>
                  </tr>

                <?php endforeach ?>
              </tbody>
            </table>
          </div>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>
<script>
   function hapus(id){
    swal({
        title: 'Are you sure?',
        text: "you won't be able to revert this!",
        type: 'warning',
        showCancelButton: true,
            // confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it'
        }).then((result) => {
            if (result.value){
                $.ajax({
                    url:'<?=base_url('form_1000/delete')?>',
                    type: "post",
                    data: {id,id},
                    success:function(){
                        swal({
                            type: "success",
                            title: "successfully",
                            text: "this data has been deleted",
                            showConfirmButton: "true"
                        }).then((result) => {
                            if(result.value){
                                location.reload();
                            }
                        });
                    }
                });
            };
        });
    }
</script>