<!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-info '">
				<div class="box-header">
					<h2 class="box-title"><b style="font-weight: 1000;font-variant: small-caps;font-size: 30px"> Tambah Data </b></h2>
					<div class="box-tools">
						<div class="margin">
						</div>
					</div>
				</div>
				<!-- /.box-header -->
				<div class="box-body" >
					<?php foreach ($data_ubah as $row): ?>
						
						<form action="<?= base_url('form_1400/update')?>" method="POST" enctype="multipart/form-data">

							<div class="form-group col-md-12">
								<label>Periode</label>
								<input type="number" id="periode" name="periode" class="form-control" value="<?= $row['periode']?>">
								<input type="hidden" id="id" name="id" class="form-control" value="<?= $row['id']?>">
							</div>

							<div class="form-group col-md-12">
								<label>Flag Detail</label>
								<input type="text" id="flag_detail" name="flag_detail" class="form-control" value="<?= $row['flag_detail']?>">
							</div>

							<div class="form-group col-md-12">
								<label>Sandi Kantor</label>
								<input type="text" id="sandi_kantor" name="sandi_kantor" class="form-control" value="<?= $row['sandi_kantor']?>">
							</div>

							<div class="form-group col-md-12">
								<label>Sandi Pos</label>
								<select name="sandi_pos" id="sandi_pos" class="form-control" onchange="setsandiposs()">
									<option value="<?= $row['sandi_pos']?>"><?= $row['sandi_pos']?></option>
									<?php foreach ($sandinya as $key): ?>
										<option value="<?= $key['sandi']?>"><?= $key['sandi']?>&nbsp&nbsp<?= $key['keterangan']?></option>
									<?php endforeach ?>
								</select>
							</div>

							<div class="form-group col-md-12">
								<label>Jumlah</label>
								<input type="number" id="jumlah" name="jumlah" class="form-control" value="<?= $row['jumlah']?>">
							</div>

							<div class="form-group col-md-12">
								<a class="btn btn-warning" href="<?= base_url('form_1400') ?>">Back</a>
								<button type="submit" class="btn btn-primary">Save</button>
							</div>

						</form>
					<?php endforeach ?>
				</div>
				<!-- /.box-body -->
			</div>
			<!-- /.box -->
		</div>
		<!-- /.col -->
	</div>
	<!-- /.row -->
</section>
<!-- <script>
	function setsandiposs() {
		var sandi = $('#sandi_pos').val();
		$.ajax({
			url: '<?= base_url("form_1400/setsandipos") ?>/' + sandi,
			type: 'POST',
			dataType: 'JSON',
			success:function(data) {
				$('#keterangan').val(data.keterangan);
			}
		});
	}
</script> -->