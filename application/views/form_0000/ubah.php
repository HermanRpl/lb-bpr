<!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-info '">
				<div class="box-header">
					<h2 class="box-title"><b style="font-weight: 1000;font-variant: small-caps;font-size: 30px"> Ubah Data </b></h2>
					<div class="box-tools">
						<div class="margin">
						</div>
					</div>
				</div>
				<!-- /.box-header -->
				<div class="box-body" >
					<?php foreach ($query as $row): ?>
						<form action="<?= base_url('form_0000/update')?>" method="POST" enctype="multipart/form-data">

							<div class="form-group col-md-6">
								<label>Periode</label>
								<input type="number" id="periode" name="periode" class="form-control" value="<?= $row['periode']?>">
								<input type="hidden" id="id" name="id" class="form-control" value="<?= $row['id']?>">
							</div>

							<div class="form-group col-md-6">
								<label>Flag Detail</label>
								<input type="text" id="flag_detail" name="flag_detail" class="form-control" value="<?= $row['flag_detail']?>">
							</div>

							<div class="form-group col-md-6">
								<label>Nama Bpr</label>
								<input type="text" id="nama_bpr" name="nama_bpr" class="form-control" value="<?= $row['nama_bpr']?>">
							</div>

							<div class="form-group col-md-6">
								<label>Alamat Bpr</label>
								<input type="text" id="alamat_bpr" name="alamat_bpr" class="form-control" value="<?= $row['alamat_bpr']?>">
							</div>

							<div class="form-group col-md-12">
								<label>Dati II Bpr</label>
								<select name="dati_ii_bpr" id="dati_ii_bpr" class="form-control" onchange="setdati()">
									<option value="<?= $row['dati_ii_bpr']?>"><?= $row['dati_ii_bpr']?></option>
									<?php foreach ($dati as $key): ?>
										<option value="<?= $key['sandi']?>"><?= $key['sandi']?>&nbsp&nbsp<?= $key['keterangan']?></option>
									<?php endforeach ?>
								</select>
							</div>

						<!-- 	<div class="form-group col-md-6">
								<label>Keterangan</label>
								<input type="text" id="keterangan" name="keterangan" class="form-control" readonly>
							</div> -->

							<div class="form-group col-md-6">
								<label>No Telepon</label>
								<input type="text" id="no_telp" name="no_telp" class="form-control" value="<?= $row['no_telp']?>">
							</div>

							<div class="form-group col-md-6">
								<label>NPWP</label>
								<input type="text" id="npwp" name="npwp" class="form-control" value="<?= $row['npwp']?>">
							</div>

							<div class="form-group col-md-12">
								<center><label>Penanggung Jawab Penyusun Laporan</label></center>
							</div>

							<div class="form-group col-md-6">
								<label>Nama</label>
								<input type="text" id="pjpl_nama" name="pjpl_nama" class="form-control" value="<?= $row['pjpl_nama']?>">
							</div>

							<div class="form-group col-md-6">
								<label>Bagian/Divisi</label>
								<input type="text" id="pjpl_bagian_divisi" name="pjpl_bagian_divisi" class="form-control" value="<?= $row['pjpl_bagian_divisi']?>">
							</div>

							<div class="form-group col-md-6">
								<label>No Telepon</label>
								<input type="text" id="pjpl_no_telp" name="pjpl_no_telp" class="form-control" value="<?= $row['pjpl_no_telp']?>">
							</div>

							<div class="form-group col-md-6">
								<label>E-Mail</label>
								<input type="email" id="pjpl_email" name="pjpl_email" class="form-control" placeholder="Wajib mengandung karakter @ dan ." value="<?= $row['pjpl_email']?>">
							</div>

							<div class="form-group col-md-12">
								<center><label>Dividen Yang Di Bayar</label></center>
							</div>

							<div class="form-group col-md-6">
								<label>Nominal</label>
								<input type="number" id="dividen_nominal" name="dividen_nominal" class="form-control" value="<?= $row['dividen_nominal']?>">
							</div>

							<div class="form-group col-md-6">
								<label>Tahun RUPS/RAT</label>
								<input type="number" id="dividen_tahun_rups" name="dividen_tahun_rups" class="form-control" value="<?= $row['dividen_tahun_rups']?>">
							</div>

							<div class="form-group col-md-12">
								<label>Bonus Tahunan Dan Tantiem</label>
								<input type="number" id="bonus_tahunan_tantiem" name="bonus_tahunan_tantiem" class="form-control" value="<?= $row['bonus_tahunan_tantiem']?>">
							</div>

							<div class="form-group col-md-12">
								<center><label>Informasi Audit Laporan Tahunan</label></center>
							</div>

							<div class="form-group col-md-6">
								<label>Nama Kantor Akuntan Yang Mengaudit</label>
								<input type="text" id="ialt_nama_kantor" name="ialt_nama_kantor" class="form-control" value="<?= $row['ialt_nama_kantor']?>">
							</div>

							<div class="form-group col-md-6">
								<label>Nama Ap Yang Menandatangani Laporan Audit</label>
								<input type="text" id="ialt_nama_ap" name="ialt_nama_ap" class="form-control" value="<?= $row['ialt_nama_ap']?>">
							</div>

							<div class="form-group col-md-12">
								<label>Pemeriksaan ke ... Dari KAP yang sama</label>
								<input type="number" id="ialt_pemeriksaan" name="ialt_pemeriksaan" class="form-control" value="<?= $row['ialt_pemeriksaan']?>">
							</div>

							<div class="form-group col-md-12">
								<label>Nilai Nominal Per Lembar Saham</label>
								<input type="number" id="nilai_nominal" name="nilai_nominal" class="form-control" value="<?= $row['nilai_nominal']?>">
							</div>

							<div class="form-group col-md-12">
								<a class="btn btn-warning" href="<?= base_url('form_0000') ?>">Back</a>
								<button type="submit" class="btn btn-primary">Save</button>
							</div>

						</form>
					<?php endforeach ?>
				</div>
				<!-- /.box-body -->
			</div>
			<!-- /.box -->
		</div>
		<!-- /.col -->
	</div>
	<!-- /.row -->
</section>
<!-- <script>
	function setdati() {
		var sandi = $('#dati_ii_bpr').val();
		$.ajax({
			url: '<?= base_url("form_0000/getdati") ?>/' + sandi,
			type: 'POST',
			dataType: 'JSON',
			success:function(data) {
				$('#keterangan').val(data.keterangan);
			}
		})	
	}
</script> -->