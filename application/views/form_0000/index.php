<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-info '">
        <div class="box-header">
          <h2 class="box-title"><b style="font-weight: 1000;font-variant: small-caps;font-size: 30px"> Form 0000 </b></h2>

          <div class="box-tools">
            <div class="margin">
              <a href="<?= base_url('form_0000/view_add') ?>">
                <span data-trigger="hover" data-container="body" data-toggle="popover" data-placement="left"  data-original-title="Tambah Data "><i class="fa fa-plus"></i></a>
                </span>
              </div>
            </div>

          </div>

          <!-- /.box-header -->
          <div class="box-body">
            <div class="table-responsive"> 
              <table id="tabel" class="table table-bordered table-hover ">
                <thead >
                  <tr class="bg-danger">
                    <th width="100" rowspan="2">Opsi</th>
                    <th rowspan="2">No</th>
                    <th rowspan="2">Periode</th>
                    <th rowspan="2" width="100px">Flag Detail</th>
                    <th rowspan="2">Nama Bpr</th>
                    <th rowspan="2">Alamat Bpr</th>
                    <th rowspan="2">Dati II Bpr</th>
                    <th rowspan="2">No Telepon</th>
                    <th rowspan="2">NPWP</th>
                    <th colspan="4"><div align="center">Penanggung Jawab Penyusun Laporan</div></th>
                    <th colspan="2"><div align="center">Dividen Yang Dibayar</div></th>
                    <th rowspan="2">Bonus Tahunan Dan Tantiem</th>
                    <th colspan="3"><div align="center">Informasi Audit Laporan Tahunan</div></th>
                    <th rowspan="2">Nilai Nominal Per Lembar Saham</th>
                  </tr>
                  <tr class="bg-danger">
                    <th>Nama</th>
                    <th>Bagian/Divisi</th>
                    <th>No Telepon</th>
                    <th>E-Mail</th>
                    <th>Nominal</th>
                    <th>Tahun RUPS/RAT</th>
                    <th>Nama Kantor Akuntan Yang Mengaudit</th>
                    <th>Nama AP Yang Menandatangani Laporan Audit</th>
                    <th>Pemeriksaan ke ... Dari KAP Yang Sama</th>
                  </tr>
                </thead>
                <tbody id="data">
                  <?php $no=1;foreach ($query as $key): ?>
                  <tr>
                    <td>
                      <div class="btn-group"> 
                        <a class="btn btn-warning" href="<?= base_url('form_0000/view_edit')?>/<?= $key['id'] ?>">
                          <i class="fa fa-edit"></i></a> 
                          <button onclick="hapus(<?= $key['id'] ?>)" type="button" class="btn btn-danger"><i class="fa fa-trash"></i></button>
                        </div>
                      </td>
                      <td><?= $no++ ?></td>
                      <td><?= $key['periode'] ?></td>
                      <td><?= $key['flag_detail'] ?></td>
                      <td><?= $key['nama_bpr'] ?></td>
                      <td><?= $key['alamat_bpr'] ?></td>
                      <td><?= $key['dati_ii_bpr'] ?></td>
                      <td><?= $key['no_telp'] ?></td>
                      <td><?= $key['npwp'] ?></td>
                      <td><?= $key['pjpl_nama'] ?></td>
                      <td><?= $key['pjpl_bagian_divisi'] ?></td>
                      <td><?= $key['pjpl_no_telp'] ?></td>
                      <td><?= $key['pjpl_email'] ?></td>
                      <td><?= $key['dividen_nominal'] ?></td>
                      <td><?= $key['dividen_tahun_rups'] ?></td>
                      <td><?= $key['bonus_tahunan_tantiem'] ?></td>
                      <td><?= $key['ialt_nama_kantor'] ?></td>
                      <td><?= $key['ialt_nama_ap'] ?></td>
                      <td><?= $key['ialt_pemeriksaan'] ?></td>
                      <td><?= $key['nilai_nominal'] ?></td>
                    </tr>
                  <?php endforeach ?>
                </tbody>
              </table>
            </div>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <script>
    function hapus(id){
      swal({
        title: 'Are you sure?',
        text: "you won't be able to revert this!",
        type: 'warning',
        showCancelButton: true,
            // confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it'
          }).then((result) => {
            if (result.value){
              $.ajax({
                url: '<?= base_url("form_0000/delete")?>',
                type: "post",
                data: {id,id},
                success:function(){
                  swal({
                    type: "success",
                    title: "successfully",
                    text: "this data has been deleted",
                    showConfirmButton: "true"
                  }).then((result) => {
                    if(result.value){
                      location.reload();
                    }
                  });
                }
              });
            };
          });
        }
      </script>