<!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-info '">
				<div class="box-header">
					<h2 class="box-title"><b style="font-weight: 1000;font-variant: small-caps;font-size: 30px"> Tambah Data </b></h2>
					<div class="box-tools">
						<div class="margin">
						</div>
					</div>
				</div>
				<!-- /.box-header -->
				<div class="box-body" >

					<form action="<?= base_url('form_0901/add')?>" method="POST" enctype="multipart/form-data">

						<div class="form-group col-md-12">
							<label>Periode</label>
							<input type="number" id="periode" name="periode" class="form-control" maxlength="6">
						</div>

						<div class="form-group col-md-12">
							<label>Flag Detail</label>
							<input type="text" id="flag_detail" name="flag_detail" class="form-control" maxlength="3">
						</div>

						<div class="form-group col-md-12">
							<label>Sandi Kantor</label>
							<input type="text" id="sandi_kantor" name="sandi_kantor" class="form-control" maxlength="2">
						</div>

						<div class="form-group col-md-12">
							<label>Uraian</label>
							<input type="text" id="uraian" name="uraian" class="form-control" maxlength="30">
						</div>


						<div class="form-group col-md-12">
							<label>Jumlah</label>
							<input type="number" id="jumlah" name="jumlah" class="form-control" maxlength="15">
						</div>

						<div class="form-group col-md-12">
							<a class="btn btn-warning" href="<?= base_url('form_0901') ?>">Back</a>
							<button type="submit" class="btn btn-primary">Save</button>
						</div>
					</form>

				</div>
				<!-- /.box-body -->
			</div>
			<!-- /.box -->
		</div>
		<!-- /.col -->
	</div>
	<!-- /.row -->
</section>
<script>
	function setsandipos() {
		var sandi = $('#sandipos').val();
		$.ajax({
			url: '<?= base_url("form_0900/setdata") ?>/' + sandi,
			type: 'POST',
			dataType:"JSON",
			success: function(data){
				$('.modal-tambah #keterangan1').val(data.keterangan);
			}
		});
	}
</script>